<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Class BaseDCATAccessCheck.
 *
 * Base class for DCAT Items related access checks.
 */
abstract class BaseDCATAccessCheck implements AccessInterface
{
  /**
   * Creates an object which extends the BaseDCATAccessCheck class.
   *
   * @param AccountInterface    $currentUser The user requesting the page
   * @param RouteMatchInterface $routeMatch  The current route
   *
   * @return static An instance that extends this class
   */
  public static function create(AccountInterface $currentUser,
                                RouteMatchInterface $routeMatch,
                                string $type,
                                string $permission): self
  {
    /** @var UserInterface $user */
    $user = User::load($currentUser->id());

    // @phpstan-ignore-next-line
    return new static(
      $user,
      $routeMatch,
      $type,
      $permission
    );
  }

  /**
   * BaseDCATAccessCheck constructor.
   *
   * @param UserInterface       $user        The user requesting the page
   * @param RouteMatchInterface $route_match The current route
   * @param string              $type        type
   * @param string              $permission  permission
   */
  public function __construct(protected UserInterface $user,
                              protected RouteMatchInterface $route_match,
                              protected string $type,
                              protected string $permission)
  {
  }
}
