<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Class CreateDCATAccessCheck.
 *
 * Access check for creating new DCAT Items.
 */
class CreateDCATAccessCheck extends BaseDCATAccessCheck
{
  /**
   * Checks if a user may create DCAT Items. This is allowed under the following
   * conditions:.
   *
   * - The user is member of at least one organization
   *
   * @return AccessResultInterface
   *                               The result of the access check
   */
  public function access(): AccessResultInterface
  {
    if ($this->user->hasPermission($this->permission)) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }
}
