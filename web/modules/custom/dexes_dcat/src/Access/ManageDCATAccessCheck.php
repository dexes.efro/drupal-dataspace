<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Class ManageDCATAccessCheck.
 *
 * Access check for managing existing DCAT Items and their distributions.
 */
class ManageDCATAccessCheck extends BaseDCATAccessCheck
{
  /**
   * Checks if a user may manage datasets. This is allowed under the following conditions:.
   *
   * - The dataset to manage exists
   * - The dataset is not managed by a consumer
   * - The user is member of the authority of the dataset OR the user has the manage all datasets permission
   *
   * @return AccessResultInterface The result of the access check
   */
  public function access(): AccessResultInterface
  {
    /** @var array<string, mixed> $dcatItem */
    $dcatItem = $this->route_match->getParameter($this->type);

    if (empty($dcatItem)) {
      return AccessResult::forbidden(sprintf(
        'Could not identify the %s that must be managed', $this->type
      ));
    }

    if (!empty($dcatItem['consumer_name'])) {
      return AccessResult::forbidden();
    }

    if ($this->user->hasPermission($this->permission)) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }
}
