<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dexes_datadeal\Exchange\ExchangeRetrieverServiceInterface;
use Drupal\dexes_dcat\Services\PolicyDetailsTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\xs_lists\ListClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ViewDatasetController.
 *
 * Controller for viewing a dataset
 */
class ViewDatasetController extends ControllerBase
{
  use PolicyDetailsTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ExchangeRetrieverServiceInterface $exchange_retriever_service */
    $exchange_retriever_service = $container->get('dexes_datadeal.exchange_retriever_service');

    /** @var ListClientInterface $list_client */
    $list_client = $container->get('xs_lists.list_client');

    return new self($exchange_retriever_service, $list_client);
  }

  /**
   * ViewDatasetController constructor.
   *
   * @param ExchangeRetrieverServiceInterface $exchange_retriever_service The service for retrieving exchange types
   * @param ListClientInterface               $listClient                 The client for interacting with XS lists
   */
  public function __construct(private readonly ExchangeRetrieverServiceInterface $exchange_retriever_service,
                              private readonly ListClientInterface $listClient)
  {
  }

  /**
   * Returns a render array that displays a dataset.
   *
   * @param array<string, mixed> $dataset The dataset to display
   *
   * @return array<string, mixed> The render array
   */
  public function viewDataset(array $dataset): array
  {
    $offers = $this->exchange_retriever_service->getOffersForDataset($dataset);
    $deals  = $this->exchange_retriever_service->getDatadealsForDataset($dataset);
    $this->addDatasetPolicyValidationRequirements($dataset);

    return [
      '#theme'           => 'dataset_detail',
      '#dataset'         => $dataset,
      '#show_controls'   => $this->userCanManage($dataset),
      '#offers'          => $offers['documents'],
      '#numOffers'       => $offers['numFound'],
      '#numDeals'        => $deals['numFound'],
      '#locale'          => $this->languageManager()->getCurrentLanguage()->getId(),
      '#is_open_license' => $this->hasOpenLicense($dataset),
      '#cache'           => [
        'keys'     => [
          'dexes-datasets',
          'dataset:' . $dataset['id'],
          'dataset:' . $dataset['name'],
          'user:' . $this->currentUser()->id(),
        ],
        'contexts' => ['user'],
        'tags'     => [
          'dexes-datasets',
          'dataset:' . $dataset['id'],
          'dataset:' . $dataset['name'],
          'user:' . $this->currentUser()->id(),
        ],
        'max-age'  => 300,
      ],
    ];
  }

  /**
   * Returns a title for a dataset.
   *
   * @param array<string, mixed> $dataset The dataset for which we return the title
   *
   * @return string The title of the dataset
   */
  public function getTitle(array $dataset): string
  {
    return $dataset['title'];
  }

  /**
   * Determines if the current user can manage the dataset being viewed.
   *
   * @param array<string, mixed> $dataset The CKAN dataset being viewed
   *
   * @return bool Whether the current user can manage the dataset
   */
  private function userCanManage(array $dataset): bool
  {
    /** @var UserInterface $user */
    $user = User::load($this->currentUser()->id());

    if (!empty($dataset['consumer_name'])) {
      return FALSE;
    }

    if ($user->hasPermission('manage all datasets')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Determines if the dataset has an open license.
   *
   * @param array<string, mixed> $dataset The Dataset being viewed
   *
   * @return bool Whether the current dataset has an open license
   */
  private function hasOpenLicense(array $dataset): bool
  {
    $license = $dataset['license_id'];

    // TODO add dexes custom license, not available atm
    $closed_licenses = [
      'http://standaarden.overheid.nl/owms/terms/geslotenlicentie',
      'http://standaarden.overheid.nl/owms/terms/geogedeeld',
      'http://standaarden.overheid.nl/owms/terms/licentieonbekend',
    ];

    return !in_array($license, $closed_licenses);
  }
}
