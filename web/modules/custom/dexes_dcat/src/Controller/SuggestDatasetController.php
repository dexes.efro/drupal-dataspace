<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\dexes_dcat\Suggest\SuggestDatasetProfile;
use Drupal\dexes_search\Controller\SuggestController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SuggestDatasetController.
 *
 * This controller returns suggestions for the "dataset" content type.
 */
class SuggestDatasetController extends SuggestController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SuggestDatasetController
  {
    /** @var SuggestDatasetProfile $suggest_profile */
    $suggest_profile = $container->get('dexes_dcat.suggest_profile.dataset');

    return parent::createController($container, $suggest_profile);
  }
}
