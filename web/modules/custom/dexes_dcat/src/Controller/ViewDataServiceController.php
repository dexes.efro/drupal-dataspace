<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Class ViewDataServiceController.
 *
 * Controller for viewing a DataService
 */
class ViewDataServiceController extends ControllerBase
{
  /**
   * Returns a render array that displays a DataService.
   *
   * @param array<string, mixed> $dataservice
   *                                          The DataService to display
   *
   * @return array<string, mixed>
   *                              The render array
   */
  public function viewDataService(array $dataservice): array
  {
    return [
      '#theme'         => 'dataservice_detail',
      '#dataservice'   => $dataservice,
      '#show_controls' => $this->userCanManage($dataservice),
      '#locale'        => $this->languageManager()->getCurrentLanguage()->getId(),
      '#cache'         => [
        'keys'     => [
          'dexes-dataservices',
          'dataservice:' . $dataservice['id'],
          'dataservice:' . $dataservice['name'],
          'user:' . $this->currentUser()->id(),
        ],
        'contexts' => ['user'],
        'tags'     => [
          'dexes-dataservices',
          'dataservice:' . $dataservice['id'],
          'dataservice:' . $dataservice['name'],
          'user:' . $this->currentUser()->id(),
        ],
        'max-age'  => 300,
      ],
    ];
  }

  /**
   * Returns a title for a dataservice.
   *
   * @param array<string, mixed> $dataservice
   *                                          The dataservice for which we return the title
   *
   * @return string
   *                The title of the dataservice
   */
  public function getTitle(array $dataservice): string
  {
    return $dataservice['title'];
  }

  /**
   * Determines if the user can manage the DataService.
   *
   * @param array<string, mixed> $dataservice
   *                                          The DataService being viewed
   *
   * @return bool
   *              Whether the user can manage the DataService
   */
  private function userCanManage(array $dataservice): bool
  {
    /** @var UserInterface $user */
    $user = User::load($this->currentUser()->id());

    if ($user->hasPermission('manage all dataservices')) {
      return TRUE;
    }

    return FALSE;
  }
}
