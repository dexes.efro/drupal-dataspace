<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\dexes_dcat\Search\SearchDatasetProfile;
use Drupal\dexes_search\Controller\SearchController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchDataServiceController.
 *
 * This controller handles all search requests that searches through
 * the "dataservice" external content type.
 */
class SearchDataServiceController extends SearchController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SearchDataServiceController
  {
    /** @var SearchDatasetProfile $search_profile */
    $search_profile = $container->get('dexes_dcat.search_profile.dataservice');

    return parent::createController($container, $search_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchableName(): string
  {
    return 'Dataservice';
  }
}
