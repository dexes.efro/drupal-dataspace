<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Services;

trait PolicyDetailsTrait
{
  /**
   * Adds the validation requirements for each resource without duplicate requirements.
   *
   * @param array $dataset The dataset to convert
   */
  private function addDatasetPolicyValidationRequirements(array &$dataset): void
  {
    $datasetPolicyValidationRequirements = [];

    $list = $this->listClient->list('DEXES:Policies');
    $list = $list->hasData() ? $list->getData() : [];

    foreach ($dataset['has_policy'] as $policy) {
      if (is_null($policy)) {
        continue;
      }

      $this->getValidationRequirements($datasetPolicyValidationRequirements, $list[$policy] ?? NULL);
    }

    foreach ($dataset['resources'] as &$resource) {
      foreach ($resource['has_policy'] as $policy) {
        if (is_null($policy)) {
          continue;
        }

        $resource['policy_validation_rules'] = $datasetPolicyValidationRequirements;
        $this->getValidationRequirements($resource['policy_validation_rules'], $list[$policy] ?? NULL);
      }
    }
  }

  /**
   * Adds the validationRequirements to the given list, if the requirement is already there skip it.
   *
   * @param array      $policyValidationRequirements The list of requirements
   * @param null|array $policy                       The policy which contains the validation requirements
   */
  private function getValidationRequirements(array &$policyValidationRequirements, ?array $policy): void
  {
    if (is_null($policy)) {
      return;
    }

    foreach ($policy['validation'] as $validation) {
      if (in_array($validation['validation_action_type'], $policyValidationRequirements)) {
        continue;
      }

      $policyValidationRequirements[$validation['validation_action_type']] = $validation;
    }
  }
}
