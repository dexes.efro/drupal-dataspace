<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\DCAT;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DCATRepository.
 *
 * Repository for retrieving and caching DCAT items from Catalog API.
 */
class DCATRepository implements DCATRepositoryInterface
{
  /**
   * The cache duration in seconds for how long DCAT items should be stored in
   * the Drupal cache for non-Dexes DCAT items.
   */
  private const CACHE_DURATION = 60 * 5;

  /**
   * The cache_key pattern.
   */
  private string $cache_pattern;

  /**
   * Creates a DCATRepository instance with the given input.
   *
   * @param APIClientInterface            $APIClient       The client for interacting with CKAN
   * @param CacheBackendInterface         $cache           The caching implementation to use
   * @param CacheTagsInvalidatorInterface $tagsInvalidator The caching implementation to use for keys
   * @param LoggerChannelFactoryInterface $loggerFactory   The logger factory to use
   * @param string                        $contentType     The content type this repository pertains to
   * @param string                        $itemType        The type of item
   *
   * @return self The created instance
   */
  public static function create(APIClientInterface $APIClient,
                                CacheBackendInterface $cache,
                                CacheTagsInvalidatorInterface $tagsInvalidator,
                                LoggerChannelFactoryInterface $loggerFactory,
                                string $contentType,
                                string $itemType): self
  {
    return new self($APIClient, $cache, $tagsInvalidator, $loggerFactory, $contentType, $itemType);
  }

  /**
   * DCATRepository constructor.
   *
   * @param APIClientInterface            $APIClient       The client for interacting with CKAN
   * @param CacheBackendInterface         $cache           The caching implementation to use
   * @param CacheTagsInvalidatorInterface $tagsInvalidator The caching implementation to use for the tags
   * @param LoggerChannelFactoryInterface $loggerFactory   The logger factory to use
   * @param string                        $contentType     The content type this repository pertains to
   * @param string                        $itemType        The type of item
   */
  public function __construct(protected APIClientInterface $APIClient,
                              private readonly CacheBackendInterface $cache,
                              private readonly CacheTagsInvalidatorInterface $tagsInvalidator,
                              protected LoggerChannelFactoryInterface $loggerFactory,
                              private readonly string $contentType,
                              private readonly string $itemType)
  {
    $this->cache_pattern = sprintf('%s:[%s]=', $this->contentType, $this->itemType) . '%s';
  }

  /**
   * {@inheritdoc}
   */
  public function loadCacheableDCATItem(string $dcat_id_or_name): ?array
  {
    $cache_key = sprintf($this->cache_pattern, $dcat_id_or_name);

    if (($cachedItem = $this->cache->get($cache_key, FALSE)) && isset($cachedItem->data)) {
      return (array) $cachedItem->data;
    }

    try {
      $DCATItem = $this->APIClient->contentType($this->contentType)->get($dcat_id_or_name)->getObject();

      if (empty($DCATItem)) {
        return NULL;
      }

      $this->cache->set(
        $cache_key, $DCATItem, time() + self::CACHE_DURATION, [
          $this->itemType . '=' . $dcat_id_or_name,
        ]
      );

      return $DCATItem;
    } catch (BaseApiException|BaseClientException $e) {
      $message = empty($e->getMessage()) ? $e::class : $e->getMessage();
      $this->loggerFactory->get('dexes_dcat')->error($message);

      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeDCATItemFromCache(string $dcat_id_or_name): void
  {
    $this->cache->delete(sprintf($this->cache_pattern, $dcat_id_or_name));

    $this->tagsInvalidator->invalidateTags([
      $this->contentType,
      sprintf('%s:%s', $this->itemType, $dcat_id_or_name),
    ]);
  }
}
