<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\DCAT;

/**
 * Class DCATUtilities.
 */
class DCATUtilities
{
  /**
   * Retrieve a resource from the dataset given its position in the resources
   * array.
   *
   * @param array $dataset
   *                       The dataset tha may contain the resource
   * @param int   $key
   *                       The position of the resource
   *
   * @return null|array
   *                    The resource of the dataset that corresponds with the key, or NULL
   */
  public static function getResourceFromDataset(array $dataset,
                                                int $key): ?array
  {
    if (!array_key_exists('resources', $dataset)) {
      return NULL;
    }

    if (!array_key_exists($key, $dataset['resources'])) {
      return NULL;
    }

    return $dataset['resources'][$key];
  }
}
