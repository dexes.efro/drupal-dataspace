<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class DatasetSitemapSearchProfile.
 *
 * Search profile for generating the sitemap.xml
 */
class DatasetSitemapSearchProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [
      'sys_type:"dataset"',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 100;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [
      'sys_name',
      'sys_modified',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'spellcheck' => FALSE,
      'facet'      => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'sys_id ASC';
  }
}
