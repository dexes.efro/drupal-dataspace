<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Search;

use Drupal\dexes_dataspace\Search\DataspaceBaseProfile;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class SearchDatasetProfile.
 */
class SearchDatasetProfile extends DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select_dataset';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return parent::standardFilters();
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return ['*'];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [
      'status'           => 'facet_status',
      'disclosure-level' => 'facet_access_rights',
      'theme'            => 'facet_theme',
      // 'classification' => 'facet_classification',
      // 'group'          => 'facet_group',
      'license'          => 'facet_license',
      'keyword'          => 'facet_keyword',
      'language'         => 'facet_sys_language',
      // 'format'         => 'facet_format',
      'frequency'        => 'facet_frequency',
      'catalog'          => 'facet_catalog',
      'authority'        => 'facet_authority',
      // 'dataspace'      => 'facet_dataspace',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return ['score DESC', 'sys_modified DESC'];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    if (!empty($parser) && $parser->isDefaultQuery()) {
      return 'sys_modified DESC';
    }

    return 'score DESC';
  }
}
