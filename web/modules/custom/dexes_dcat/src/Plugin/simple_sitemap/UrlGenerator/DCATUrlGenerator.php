<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Plugin\simple_sitemap\UrlGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\UrlGeneratorBase;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use RuntimeException;

abstract class DCATUrlGenerator extends UrlGeneratorBase
{
  /**
   * The service for retrieving all the datasets.
   */
  protected SearchServiceInterface $search_service;

  /**
   * The profile for retrieving potential sitemap entries.
   */
  protected SearchProfileInterface $search_profile;

  /**
   * {@inheritdoc}
   */
  public function getDataSets(): array
  {
    $datasets = [];

    do {
      $response = $this->search_service->search(
        $this->search_profile, '*:*', [], count($datasets),
        $this->search_profile->sort()
      );

      if (NULL === $response) {
        throw new RuntimeException(
          'Failed to query the search-engine for sitemap entries'
        );
      }

      $response = $response->getSolrResponse()->getResponse();

      if (NULL === $response) {
        throw new RuntimeException(
          'Failed to fetch the SolrResponse from the search query'
        );
      }

      foreach ($response->getDocuments() as $document) {
        $datasets[] = [
          'name'     => $document->getStringField('sys_name'),
          'modified' => $document->getStringField('sys_modified'),
        ];
      }

      $got_all = count($datasets) >= $response->getNumFound();
    } while (!$got_all);

    return $datasets;
  }
}
