<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataset;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\ClientConflictException;
use XpertSelect\ApiClient\Payload;

/**
 * Class AddDatasetForm.
 *
 * Form for creating new datasets in the Catalog API.
 */
class AddDatasetForm extends DatasetBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_add_dataset_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form = $this->buildDatasetForm($form_state, title: $this->t('Add a dataset'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type'          => 'submit',
      '#value'         => $this->t('Add dataset'),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_search.search.dataset', [
        'scope'       => 'dataset',
        'query'       => '-',
        'filters'     => '-',
        'page_number' => 1,
      ]),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3',
          'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $newDataset = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS);

    $identifierPrefix = $this->getIdentifierPrefix();

    $newDataset['identifier'] = array_key_exists('identifier', $newDataset)
      ? $newDataset['identifier']
      : Url::fromUri($identifierPrefix . $newDataset['name'])
        ->toString();

    $newDataset['resources']      = [];
    $newDataset['dataspace_uri']  = $this->dataspaceConfigurationService->get('uri');
    $newDataset['source_catalog'] = $this->dataspaceConfigurationService->get('uri');
    $this->normalizePolicies($newDataset);

    try {
      $payload = new Payload();
      $payload->addValues($newDataset);
      $response = $this->APIClient->contentType('dexes-datasets')->store($payload);

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.resource.add', [
          'dataset' => $newDataset['name'],
        ]
      ));
    } catch (ClientConflictException|BaseApiException|ClientException $e) {
      $this->messenger()
        ->addError(t('A system error prevented the creation of the dataset.'));
      $form_state->setRebuild();

      return;
    }
  }

  /**
   * Get the identifier prefix. If no identifier_prefix value for datasets is set it will return the dataspace uri.
   */
  private function getIdentifierPrefix(): string
  {
    $prefix = $this->dataspaceConfigurationService->get('uri');

    if (!str_ends_with($prefix, '/')) {
      $prefix .= '/';
    }

    return $prefix . 'dataset/';
  }
}
