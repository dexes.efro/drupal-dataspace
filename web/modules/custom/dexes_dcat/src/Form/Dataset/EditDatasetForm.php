<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataset;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;
use XpertSelect\ApiClient\Payload;

/**
 * Class EditDatasetForm.
 *
 * Edit form for datasets.
 */
class EditDatasetForm extends DatasetBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_edit_dataset_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
                            ?array $dataset = NULL): array
  {
    if (empty($dataset)) {
      throw new NotFoundHttpException();
    }

    $form = $this->buildDatasetForm($form_state, $dataset, $this->t('Edit a dataset'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Save dataset'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_dcat.dataset.view', [
        'dataset' => $dataset['name'],
      ]),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3', 'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $dataset = $this->getRouteMatch()->getParameter('dataset');

    if (empty($dataset)) {
      throw new NotFoundHttpException();
    }

    $newDataset = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS);

    $newDataset['resources']      = $dataset['resources'] ?? [];
    $newDataset['dataspace_uri']  = $this->dataspaceConfigurationService->get('uri');
    $newDataset['source_catalog'] = $this->dataspaceConfigurationService->get('uri');
    $this->normalizePolicies($newDataset);

    try {
      $payload = new Payload();
      $payload->addValues($newDataset);
      $response = $this->APIClient->contentType('dexes-datasets')
        ->update($dataset['id'], $payload);

      $this->repository->removeDCATItemFromCache($dataset['name']);

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.dataset.view', [
          'dataset' => $dataset['name'],
        ]
      ));
    } catch (BaseApiException|BaseClientException|ClientException $e) {
      $this->messenger()
        ->addError(t('A system error prevented updating the dataset.'));
      $form_state->setRebuild();

      return;
    }
  }
}
