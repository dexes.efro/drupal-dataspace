<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataset;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Drupal\dexes_dcat\Form\DCATFormBase;
use Drupal\user\UserStorageInterface;
use Drupal\xs_lists\ListClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DatasetBaseForm.
 *
 * This is a base form for dataset related forms.
 */
abstract class DatasetBaseForm extends DCATFormBase
{
  /**
   * The list of CKAN dataset fields in the form and needed by CKAN.
   *
   * @var string[]
   */
  protected const FIELDS = [
    'title',
    'name',
    'notes',
    'theme',
    'tag_string',
    'url',
    'authority',
    'contact_point_name',
    'contact_point_email',
    'contact_point_website',
    'contact_point_phone',
    'access_rights',
    'dataset_status',
    'has_policy',
    'issued',
    'modified',
    'license_id',
    'restriction_statement',
    'legal_foundation_label',
    'legal_foundation_uri',
    'legal_foundation_ref',
    'provenance',
    'spatial_scheme',
    'spatial_value',
    'temporal_start',
    'temporal_end',
    'temporal_label',
    'source',
    'related_resource',
    'is_version_of',
    'has_version',
    'documentation',
    'conforms_to',
    'sample',
    'version',
    'version_notes',
    'frequency',
    'language',
    'metadata_language',
    'identifier',
    'alternate_identifier',
    'publisher',
    'source_catalog',
  ];

  /**
   * The list of DateTime fields in the form and needed by CKAN.
   *
   * @var string[]
   */
  protected const DATETIME_FIELDS = [
    'issued',
    'modified',
    'temporal_start',
    'temporal_end',
  ];

  /**
   * A list of multivalued select fields. These values need a special treatment
   * before sending to CKAN.
   *
   * @var string[]
   */
  protected const MULTIVALUED_SELECT_FIELDS = [
    'theme',
    'language',
    'has_policy',
  ];

  /**
   * The dateformat used by datasets.
   */
  protected const DATASET_DATE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DatasetBaseForm
  {
    /** @var DCATRepositoryInterface $datasetRepository */
    $datasetRepository = $container->get('dexes_dcat.dataset_repository');

    /** @var APIClientInterface $APIClient */
    $APIClient = $container->get('xs_api_client.api_client_default');

    /** @var ListClientInterface $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');

    /** @var UserStorageInterface $userStorage */
    $userStorage = $entityTypeManager->getStorage('user');

    /** @var DataspaceConfigurationService $dataspaceConfigurationService */
    $dataspaceConfigurationService = $container->get('dexes_dataspace.configuration_service');

    // @phpstan-ignore-next-line
    return new static(
      $datasetRepository,
      $APIClient,
      $listClient,
      $userStorage,
      $dataspaceConfigurationService
    );
  }

  /**
   * DatasetBaseForm constructor.
   *
   * @param DCATRepositoryInterface       $datasetRepository             The dataset repository to load datasets from cache
   * @param APIClientInterface            $APIClient                     The CKAN client to communicate with Catalog API
   * @param ListClientInterface           $listClient                    The list client to retrieve lists (for select fields)
   * @param UserStorageInterface          $userStorage                   The user storage
   * @param DataspaceConfigurationService $dataspaceConfigurationService The service that holds the dataspace configuration
   */
  public function __construct(DCATRepositoryInterface $datasetRepository,
                              APIClientInterface $APIClient,
                              ListClientInterface $listClient,
                              UserStorageInterface $userStorage,
                              protected DataspaceConfigurationService $dataspaceConfigurationService)
  {
    parent::__construct($datasetRepository, $listClient, $APIClient, $userStorage, $dataspaceConfigurationService);
  }

  /**
   * Builds the dataset form.
   *
   * @param FormStateInterface   $form_state
   *                                         The state of the form
   * @param array<string, mixed> $dataset
   *                                         The optional dataset when editing an existing dataset
   * @param string               $title
   *                                         The title of the form
   *
   * @return array<string, mixed>
   *                              The form definition
   */
  public function buildDatasetForm(FormStateInterface $form_state, array $dataset = [], string $title = ''): array
  {
    return $this->buildTitlePartOfForm($title)
      + $this->buildDescriptionPartOfForm($dataset)
      + $this->buildOwnerPartOfForm($dataset)
      + $this->buildDataSharingPartOfForm($form_state, $dataset)
      + $this->buildLocationAndTimePartOfForm($dataset)
      + $this->buildRelationsPartOfForm($dataset)
      + $this->buildLegalPartOfForm($dataset)
      + $this->buildExtrasPartOfForm($dataset);
  }

  /**
   * Checks whether a given name/id exists in the Catalog API.
   *
   * @param string              $value      The name/id
   * @param array<mixed, mixed> $element    The element definition where the name/id is coming from
   * @param FormStateInterface  $form_state The state of the form
   *
   * @return bool
   *              A boolean indicating whether the given name/id exists in the Catalog API
   */
  public function exists(string $value, array $element,
                         FormStateInterface $form_state): bool
  {
    try {
      $array = $this->APIClient->contentType('dexes-datasets')->get($value)->getObject();

      return !(empty($array));
    } catch (BaseApiException|BaseClientException|ClientException $e) {
      return FALSE;
    }
  }

  /**
   * Callback for both ajax-enabled buttons for the policy. Selects and returns the fieldset with the names in it.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   *
   * @return array<string, mixed> The modified $form
   */
  public function alterMoreCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['datasharing']['policy']['has_policy'];
  }

  /**
   * Builds the description part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the description part of the dataset form
   */
  private function buildDescriptionPartOfForm(array $dataset = []): array
  {
    $form['description'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Description'),
      '#open'        => TRUE,
    ];

    $form['description']['title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Title'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('title', $dataset)
        ? $dataset['title'] : NULL,
    ];

    $form['description']['name'] = [
      '#type'          => 'machine_name',
      '#title'         => $this->t('Machine name'),
      '#machine_name'  => [
        'source'          => ['description', 'title'],
        'replace_pattern' => self::REPLACE_PATTERN,
        'replace'         => self::REPLACE_TOKEN,
        'exists'          => [$this, 'exists'],
      ],
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('name', $dataset)
        ? $dataset['name'] : NULL,
      '#disabled'      => array_key_exists('name', $dataset),
    ];

    $form['description']['notes'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('notes', $dataset)
        ? $dataset['notes'] : NULL,
    ];

    $form['description']['theme'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Themes'),
      '#multiple'      => TRUE,
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#select2'       => [
        'multiple' => TRUE,
      ],
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:TaxonomieBeleidsagenda')),
      '#default_value' => array_key_exists('theme', $dataset)
        ? $dataset['theme'] : NULL,
    ];

    $form['description']['tag_string'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Tags'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('tags', $dataset)
        ? implode(',', array_map(function($tag) {
          return $tag['display_name'];
        }, $dataset['tags'])) : NULL,
    ];

    $form['description']['url'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Landing page'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('url', $dataset)
        ? $dataset['url'] : NULL,
    ];

    // TODO
//    $form['description']['classifications'] = [
//      '#type' => 'select',
//      '#title' => $this->t('Data classifications'),
//      '#multiple' => TRUE,
//      '#options' => $this->list_client->formatter()
//        ->flatten($this->list_client->list('classifications')),
//    ];

    return $form;
  }

  /**
   * Builds the owner part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the owner part of the dataset form
   */
  private function buildOwnerPartOfForm(array $dataset = []): array
  {
    $form['owner'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Owner'),
      '#description' => $this->t('Information about the owner.'),
      '#open'        => FALSE,
    ];

    $form['owner']['authority'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Data owner'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#options'       => $this->filterOrganizationsByUser(),
      '#default_value' => array_key_exists('authority', $dataset)
        ? $dataset['authority'] : NULL,
    ];

    $form['owner']['publisher'] = [
      '#type'          => 'select2',
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Organization')),
      '#title'         => $this->t('Publishing organization'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('publisher', $dataset)
        ? $dataset['publisher'] : NULL,
    ];

    $form['owner']['contact'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Contact point'),
      '#description' => $this->t('At least one of e-mail, website or phone is required.'),
    ];

    $form['owner']['contact']['contact_point_name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Department'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('contact_point_name', $dataset)
        ? $dataset['contact_point_name'] : NULL,
    ];

    $form['owner']['contact']['contact_point_email'] = [
      '#type'          => 'email',
      '#title'         => $this->t('E-mail'),
      '#attributes'    => [
        'name'  => 'contact_point_email',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point_website"]' => ['filled' => FALSE],
          ':input[name="contact_point_phone"]'   => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('contact_point_email', $dataset)
        ? $dataset['contact_point_email'] : NULL,
    ];

    $form['owner']['contact']['contact_point_website'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Website'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'name'  => 'contact_point_website',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point_email"]' => ['filled' => FALSE],
          ':input[name="contact_point_phone"]' => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('contact_point_website', $dataset)
        ? $dataset['contact_point_website'] : NULL,
    ];

    $form['owner']['contact']['contact_point_phone'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Phone number'),
      '#attributes'    => [
        'name'  => 'contact_point_phone',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point_email"]'   => ['filled' => FALSE],
          ':input[name="contact_point_website"]' => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('contact_point_phone', $dataset)
        ? $dataset['contact_point_phone'] : NULL,
    ];

    return $form;
  }

  /**
   * Builds the data sharing part of the form.
   */
  private function buildDataSharingPartOfForm(FormStateInterface $form_state, array $dataset = []): array
  {
    $form['datasharing'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Data sharing'),
      '#open'        => FALSE,
    ];

    $form['datasharing']['access_availability'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Access and availability'),
    ];

    $form['datasharing']['access_availability']['access_rights'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Access'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:Openbaarheidsniveau')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('access_rights', $dataset)
        ? $dataset['access_rights'] : NULL,
    ];

    $form['datasharing']['access_availability']['dataset_status'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Status'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:DatasetStatus')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('dataset_status', $dataset)
        ? $dataset['dataset_status'] : NULL,
    ];

    $form['datasharing']['license_and_conditions'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('License and conditions'),
    ];

    $form['datasharing']['license_and_conditions']['license_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('License'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:License')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('license_id', $dataset)
        ? $dataset['license_id'] : NULL,
    ];

    $form['datasharing']['license_and_conditions']['restriction_statement'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('License explanation'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('restriction_statement', $dataset)
        ? $dataset['restriction_statement'] : NULL,
    ];

    $form['datasharing']['policy'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Policies and dataspaces'),
    ];

    $this->buildPolicyPartOfForm(
      $form_state,
      $form,
      $form['datasharing']['policy'],
      $this->getSelectedPolicies($form_state, $dataset)
    );

    return $form;
  }

  /**
   * Builds the location and time part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the location and time part of the dataset form
   */
  private function buildLocationAndTimePartOfForm(array $dataset = []): array
  {
    $form['location_time'] = [
      '#type'  => 'details',
      '#title' => $this->t('Location & time'),
      '#open'  => FALSE,
    ];

    $form['location_time']['geo'] = [
      '#type'       => 'fieldset',
      '#title'      => $this->t('Geographic data'),
    ];

    $form['location_time']['geo']['spatial_scheme'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Geographic type'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:SpatialScheme')),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#empty_value'   => '',
      '#default_value' => array_key_exists('spatial_scheme', $dataset)
        ? $dataset['spatial_scheme'] : NULL,
    ];

    $form['location_time']['geo']['spatial_value'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Geographic value'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('spatial_value', $dataset)
        ? $dataset['spatial_value'] : NULL,
    ];

    $form['location_time']['time'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Time coverage'),
    ];

    $form['location_time']['time']['temporal_start'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Begin date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('temporal_start', $dataset)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataset['temporal_start'])
        : NULL,
    ];

    $form['location_time']['time']['temporal_end'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('End date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('temporal_end', $dataset)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataset['temporal_end'])
        : NULL,
    ];

    $form['location_time']['time']['temporal_label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Coverage name'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('temporal_label', $dataset)
        ? $dataset['temporal_label'] : NULL,
    ];

    return $form;
  }

  /**
   * Builds the relations part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the relations part of the dataset form
   */
  private function buildRelationsPartOfForm(array $dataset = []): array
  {
    $form['relations'] = [
      '#type'  => 'details',
      '#title' => $this->t('Relations'),
      '#open'  => FALSE,
    ];

    $form['relations']['source'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Sources'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('source', $dataset)
        ? $dataset['source'] : NULL,
    ];

    $form['relations']['related_resource'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Related resources'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('related_resource', $dataset)
        ? $dataset['related_resource'] : NULL,
    ];

    $form['relations']['is_version_of'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Based on'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('is_version_of', $dataset)
        ? $dataset['is_version_of'] : NULL,
    ];

    $form['relations']['has_version'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Others based on this'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('has_version', $dataset)
        ? $dataset['has_version'] : NULL,
    ];

    $form['relations']['documentation'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Documentation'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('documentation', $dataset)
        ? $dataset['documentation']
        : NULL,
    ];

    $form['relations']['conforms_to'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Standards'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('conforms_to', $dataset)
        ? $dataset['conforms_to'] : NULL,
    ];

    $form['relations']['sample'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Examples'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('sample', $dataset)
        ? $dataset['sample'] : NULL,
    ];

    return $form;
  }

  /**
   * Builds the legal part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the legal part of the dataset form
   */
  private function buildLegalPartOfForm(array $dataset = []): array
  {
    $form['legal'] = [
      '#type'  => 'details',
      '#title' => $this->t('Legal'),
      '#open'  => FALSE,
    ];

    $form['legal']['base'] = [
      '#type'       => 'fieldset',
      '#title'      => $this->t('Legal base'),
    ];

    $form['legal']['base']['legal_foundation_label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Cite title'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('legal_foundation_label', $dataset)
        ? $dataset['legal_foundation_label'] : NULL,
    ];

    $form['legal']['base']['legal_foundation_uri'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Link'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('legal_foundation_uri', $dataset)
        ? $dataset['legal_foundation_uri'] : NULL,
    ];

    $form['legal']['base']['legal_foundation_ref'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Juriconnect reference'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('legal_foundation_ref', $dataset)
        ? $dataset['legal_foundation_ref'] : NULL,
    ];

    $form['legal']['provenance'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Goal'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('provenance', $dataset)
        ? $dataset['provenance'] : NULL,
    ];

    return $form;
  }

  /**
   * Builds the extras part of the dataset form.
   *
   * @param array<string, mixed> $dataset The optional dataset when editing an existing dataset
   *
   * @return array<string, array> The definition of the extras part of the dataset form
   */
  private function buildExtrasPartOfForm(array $dataset = []): array
  {
    $form['extras'] = [
      '#type'  => 'details',
      '#title' => $this->t('Extra metadata'),
      '#open'  => FALSE,
    ];

    $form['extras']['version_control'] = [
      '#type'       => 'fieldset',
      '#title'      => $this->t('Version control'),
    ];

    $form['extras']['version_control']['version'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Version'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('version', $dataset)
        ? $dataset['version'] : NULL,
    ];

    $form['extras']['version_control']['version_notes'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Version description'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('version_notes', $dataset)
        ? $dataset['version_notes'] : NULL,
    ];

    $form['extras']['version_control']['frequency'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Change frequency'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:Frequency')),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#empty_value'   => '',
      '#default_value' => array_key_exists('frequency', $dataset)
        ? $dataset['frequency'] : NULL,
    ];

    $form['extras']['language_settings'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Language settings'),
    ];

    $form['extras']['language_settings']['language'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Language dataset'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Language')),
      '#required'      => TRUE,
      '#multiple'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('language', $dataset)
        ? $dataset['language'] : NULL,
    ];

    $form['extras']['language_settings']['metadata_language'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Language metadata'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Language')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('metadata_language', $dataset)
        ? $dataset['metadata_language'] : NULL,
    ];

    $form['extras']['identifiers'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Identifiers'),
    ];

    $form['extras']['identifiers']['identifier'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Identifier'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#disabled'      => array_key_exists('identifier', $dataset),
      '#default_value' => array_key_exists('identifier', $dataset)
        ? $dataset['identifier'] : NULL,
    ];

    $form['extras']['identifiers']['alternate_identifier'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Alternative identifier'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('alternate_identifier', $dataset)
        ? $dataset['alternate_identifier'] : NULL,
    ];

    $form['extras']['reuse'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Date and time'),
    ];

    $form['extras']['reuse']['issued'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Date issued'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('issued', $dataset)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataset['issued'])
        : NULL,
    ];

    $form['extras']['reuse']['modified'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Date modified'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('modified', $dataset)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataset['modified'])
        : DrupalDateTime::createFromTimestamp(time()),
    ];

    return $form;
  }
}
