<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Resource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;
use XpertSelect\ApiClient\Payload;

/**
 * Class AddResourcesForm.
 *
 * Form for adding resources to a dataset
 */
class AddResourceForm extends ResourceBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_add_resource_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
                            ?array $dataset = NULL): array
  {
    if (empty($dataset)) {
      throw new NotFoundHttpException();
    }

    $form = $this->buildResourceForm($form_state, title: $this->t('Add a distribution'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['submit'] = [
      '#type'          => 'submit',
      '#value'         => t('Add distribution'),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_dcat.dataset.view', [
        'dataset' => $dataset['name'],
      ]),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3',
          'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $dataset = $this->getRouteMatch()->getParameter('dataset');

    if (empty($dataset)) {
      throw new NotFoundHttpException();
    }

    $new_resource = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS);

    $new_resource['package_id'] = $dataset['id'];
    $this->normalizePolicies($new_resource);

    $dataset['resources'][] = $new_resource;

    try {
      $payload = new Payload();
      $payload->addValues($dataset);
      $response = $this->APIClient->contentType('dexes-datasets')->update($dataset['id'], $payload);

      $this->repository->removeDCATItemFromCache($dataset['name']);

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.dataset.view', [
          'dataset' => $dataset['name'],
        ]
      ));
    } catch (BaseClientException|BaseApiException|ClientException $e) {
      $this->messenger()
        ->addError(t('A system error prevented the creation of the distribution.'));
      $form_state->setRebuild();

      return;
    }
  }
}
