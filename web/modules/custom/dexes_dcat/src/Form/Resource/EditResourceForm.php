<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Resource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dexes_dcat\DCAT\DCATUtilities;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;
use XpertSelect\ApiClient\Payload;

/**
 * Class EditResourceForm.
 *
 * Form for editing resources of a dataset
 */
class EditResourceForm extends ResourceBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_edit_resource_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
                            ?array $dataset = NULL,
                            ?string $resource_id = NULL): array
  {
    $resource_id = $this->getResourceIdFromRoute();

    if (empty($dataset) || NULL === $resource_id) {
      throw new NotFoundHttpException();
    }

    $resource = DCATUtilities::getResourceFromDataset($dataset, $resource_id);

    if (empty($resource)) {
      throw new NotFoundHttpException();
    }

    $form = $this->buildResourceForm($form_state, $resource, $this->t('Edit a distribution'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type'          => 'submit',
      '#value'         => t('Save distribution'),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_dcat.dataset.view', [
        'dataset' => $dataset['name'],
      ]),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3',
          'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $dataset     = $this->getRouteMatch()->getParameter('dataset');
    $resource_id = $this->getResourceIdFromRoute();

    if (empty($dataset) || NULL === $resource_id) {
      throw new NotFoundHttpException();
    }

    $resource = DCATUtilities::getResourceFromDataset($dataset, $resource_id);

    if (is_null($resource)) {
      throw new NotFoundHttpException();
    }

    $new_resource = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS
    );

    $resource['package_id'] = $dataset['id'];
    $this->normalizePolicies($new_resource);

    $dataset['resources'][$resource_id] = $new_resource;

    try {
      $payload = new Payload();
      $payload->addValues($dataset);
      $response = $this->APIClient->contentType('dexes-datasets')
        ->update($dataset['id'], $payload);

      $this->repository->removeDCATItemFromCache($dataset['name']);

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.dataset.view', [
          'dataset' => $dataset['name'],
        ]
      ));
    } catch (BaseApiException|BaseClientException|ClientException $e) {
      $this->messenger()
        ->addError(t('A system error prevented updating the distribution.'));
      $form_state->setRebuild();

      return;
    }
  }
}
