<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Resource;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Drupal\dexes_dcat\Form\DCATFormBase;
use Drupal\user\UserStorageInterface;
use Drupal\xs_lists\ListClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\ApiClient\APIClientInterface;

/**
 * Class ResourceBaseForm.
 *
 * This is a base form for resource related forms.
 */
abstract class ResourceBaseForm extends DCATFormBase
{
  /**
   * The list of resource fields in the form.
   *
   * @var string[]
   */
  protected const FIELDS = [
    'url',
    'download_url',
    'documentation',
    'linked_schemas',
    'name',
    'description',
    'status',
    'metadata_language',
    'language',
    'license_id',
    'has_policy',
    'rights',
    'format',
    'media_type',
    'size',
    'hash',
    'hash_algorithm',
    'release_date',
    'modification_date',
    'distribution_type',
  ];

  /**
   * The list of DateTime fields in the form.
   *
   * @var string[]
   */
  protected const DATETIME_FIELDS = [
    'release_date',
    'modification_date',
  ];

  /**
   * A list of multivalued select fields. These values need a special treatment.
   *
   * @var string[]
   */
  protected const MULTIVALUED_SELECT_FIELDS = [
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ResourceBaseForm
  {
    /** @var DCATRepositoryInterface $datasetRepository */
    $datasetRepository = $container->get('dexes_dcat.dataset_repository');

    /** @var APIClientInterface $APIClient */
    $APIClient = $container->get('xs_api_client.api_client_default');

    /** @var ListClientInterface $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');

    /** @var UserStorageInterface $userStorage */
    $userStorage = $entityTypeManager->getStorage('user');

    /** @var DataspaceConfigurationService $dataspaceConfigurationService */
    $dataspaceConfigurationService = $container->get('dexes_dataspace.configuration_service');

    // @phpstan-ignore-next-line
    return new static(
      $datasetRepository,
      $APIClient,
      $listClient,
      $userStorage,
      $dataspaceConfigurationService
    );
  }

  /**
   * DatasetBaseForm constructor.
   *
   * @param DCATRepositoryInterface       $datasetRepository             The dataset repository to load datasets from cache
   * @param APIClientInterface            $APIClient                     The CKAN client to communicate with Catalog API
   * @param ListClientInterface           $listClient                    The list client to retrieve lists (for select fields)
   * @param UserStorageInterface          $userStorage                   The user storage
   * @param DataspaceConfigurationService $dataspaceConfigurationService The service that holds the dataspace configuration
   */
  public function __construct(DCATRepositoryInterface $datasetRepository,
                              APIClientInterface $APIClient,
                              ListClientInterface $listClient,
                              UserStorageInterface $userStorage,
                              protected DataspaceConfigurationService $dataspaceConfigurationService)
  {
    parent::__construct($datasetRepository, $listClient, $APIClient, $userStorage, $dataspaceConfigurationService);
  }

  /**
   * Callback for both ajax-enabled buttons for the policy. Selects and returns the fieldset with the names in it.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   *
   * @return array<string, mixed> The modified $form
   */
  public function alterMoreCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['policy']['has_policy'];
  }

  /**
   * Builds the resource form.
   *
   * @param array<string, mixed> $resource
   *                                       The resource when editing an existing resource
   *
   * @return array<string, mixed>
   *                              The form definition
   */
  protected function buildResourceForm(FormStateInterface $form_state, array $resource = [], string $title = ''): array
  {
    $form = $this->buildTitlePartOfForm($title);

    $form['description'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Description'),
      '#open'        => TRUE,
    ];

    $form['description']['name'] = [
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#title'         => t('Name'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('name', $resource)
        ? $resource['name']
        : NULL,
    ];

    $form['description']['description'] = [
      '#type'          => 'textarea',
      '#required'      => TRUE,
      '#title'         => t('Description'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('description', $resource)
        ? $resource['description']
        : NULL,
    ];

    $form['data'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Data'),
      '#open'        => TRUE,
    ];

    $form['data']['url'] = [
      '#type'          => 'url',
      '#placeholder'   => 'https://',
      '#required'      => TRUE,
      '#title'         => t('URL'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('url', $resource)
        ? $resource['url']
        : NULL,
    ];

    $form['data']['format'] = [
      '#type'          => 'select2',
      '#title'         => t('Format'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('MDR:Filetype')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('format', $resource)
        ? $resource['format']
        : NULL,
    ];

    $form['data']['media_type'] = [
      '#type'          => 'select2',
      '#title'         => t('Media type'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('IANA:MediaTypes')),
      '#empty_value'   => '',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('media_type', $resource)
        ? $resource['media_type']
        : NULL,
    ];

    // TODO: multivalued
    $form['data']['download_url'] = [
      '#type'          => 'url',
      '#placeholder'   => 'https://',
      '#title'         => t('Download URL'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('download_url', $resource)
        ? $resource['download_url']
        : NULL,
    ];

    // TODO: multivalued
    $form['data']['documentation'] = [
      '#type'          => 'url',
      '#placeholder'   => 'https://',
      '#title'         => t('Documentation'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('documentation', $resource)
        ? $resource['documentation']
        : NULL,
    ];

    // TODO: multivalued
    $form['data']['linked_schemas'] = [
      '#type'          => 'url',
      '#placeholder'   => 'https://',
      '#title'         => t('Linked schemas'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('linked_schemas', $resource)
        ? $resource['linked_schemas']
        : NULL,
    ];

    $form['data']['size'] = [
      '#type'          => 'number',
      '#title'         => t('Size'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('size', $resource)
        ? $resource['size']
        : NULL,
    ];

    $form['data']['hash'] = [
      '#type'          => 'textfield',
      '#title'         => t('Hash'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('hash', $resource)
        ? $resource['hash']
        : NULL,
    ];

    $form['data']['hash_algorithm'] = [
      '#type'          => 'textfield',
      '#title'         => t('Hash algorithm'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('hash_algorithm', $resource)
        ? $resource['hash_algorithm']
        : NULL,
    ];

    $form['datasharing'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Data sharing'),
      '#open'        => TRUE,
    ];

    $form['datasharing']['status'] = [
      '#type'          => 'select',
      '#title'         => t('Status'),
      '#required'      => TRUE,
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('ADMS:DistributieStatus')),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#empty_value'   => '',
      '#default_value' => array_key_exists('status', $resource)
        ? $resource['status']
        : NULL,
    ];

    $form['datasharing']['license_id'] = [
      '#type'          => 'select',
      '#title'         => t('License'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:License')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('license_id', $resource)
        ? $resource['license_id']
        : NULL,
    ];

    $form['datasharing']['policy'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Policies'),
    ];

    $this->buildPolicyPartOfForm(
      $form_state,
      $form,
      $form['datasharing']['policy'],
      $this->getSelectedPolicies($form_state, $resource)
    );

    $form['extra'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Extra metadata'),
      '#open'        => TRUE,
    ];

    $form['extra']['metadata_language'] = [
      '#type'          => 'select',
      '#title'         => t('Metadata language'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Language')),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#required'      => TRUE,
      '#default_value' => array_key_exists('metadata_language', $resource)
        ? $resource['metadata_language']
        : NULL,
    ];

    $form['extra']['language'] = [
      '#type'          => 'select',
      '#title'         => t('Language'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Language')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#multiple'      => TRUE,
      '#default_value' => array_key_exists('language', $resource)
        ? $resource['language']
        : NULL,
    ];

    $form['extra']['rights'] = [
      '#type'          => 'textfield',
      '#title'         => t('Rights'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('rights', $resource)
        ? $resource['rights']
        : NULL,
    ];

    $form['extra']['release_date'] = [
      '#type'          => 'datetime',
      '#title'         => t('Release date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('release_date', $resource)
        ? DrupalDateTime::createFromFormat('Y-m-d\TH:i:s', $resource['release_date'])
        : NULL,
    ];

    $form['extra']['modification_date'] = [
      '#type'          => 'datetime',
      '#title'         => t('Modification date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('modification_date', $resource)
        ? DrupalDateTime::createFromFormat('Y-m-d\TH:i:s', $resource['modification_date'])
        : NULL,
    ];

    $form['extra']['distribution_type'] = [
      '#type'          => 'select',
      '#title'         => t('Distribution type'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:DistributionType')),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#empty_value'   => '',
      '#default_value' => array_key_exists('distribution_type', $resource)
        ? $resource['distribution_type']
        : NULL,
    ];

    return $form;
  }

  /**
   * Get the resource id from the route and make in 0-based index.
   *
   * @return ?int
   */
  protected function getResourceIdFromRoute(): ?int
  {
    $resource_id = $this->getRouteMatch()->getParameter('resource_id');

    if (NULL === $resource_id) {
      return NULL;
    }

    return $resource_id;
  }
}
