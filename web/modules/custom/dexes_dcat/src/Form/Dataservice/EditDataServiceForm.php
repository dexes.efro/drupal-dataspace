<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataservice;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;
use XpertSelect\ApiClient\Payload;

/**
 * Class EditDataServiceForm.
 */
class EditDataServiceForm extends DataServiceBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_edit_dataservice_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
                            ?array $dataservice = NULL): array
  {
    if (empty($dataservice)) {
      throw new NotFoundHttpException();
    }

    $form = $this->buildDataServiceForm($form_state, $dataservice, $this->t('Edit a dataservice'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['submit'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Save DataService'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_dcat.dataservice.view', [
        'dataservice' => $dataservice['name'],
      ]),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3', 'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $dataService = $this->getRouteMatch()->getParameter('dataservice');

    if (empty($dataService)) {
      throw new NotFoundHttpException();
    }

    $newDataService = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS);

    $this->normalizePolicies($newDataService);

    $newDataService['dataspace_uri'] = $this->dataspaceConfigurationService->get('uri');

    try {
      $payload = new Payload();
      $payload->addValues($newDataService);
      $response = $this->APIClient->contentType('dexes-dataservices')
        ->update($dataService['id'], $payload);

      $this->repository->removeDCATItemFromCache($dataService['name']);

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.dataservice.view', [
          'dataservice' => $dataService['name'],
        ]
      ));
    } catch (BaseApiException|BaseClientException|ClientException) {
      $this->messenger()
        ->addError(t('A system error prevented updating the DataService.'));
      $form_state->setRebuild();

      return;
    }
  }
}
