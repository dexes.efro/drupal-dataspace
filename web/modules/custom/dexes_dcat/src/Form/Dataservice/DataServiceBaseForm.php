<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataservice;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Drupal\dexes_dcat\Form\DCATFormBase;
use Drupal\user\UserStorageInterface;
use Drupal\xs_lists\ListClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DataServiceBaseForm.
 */
abstract class DataServiceBaseForm extends DCATFormBase
{
  /**
   * The list of DataService fields in the form and required by the catalog api.
   *
   * @var string[]
   */
  protected const FIELDS = [
    'name',
    'title',
    'access_rights',
    'conforms_to',
    'contact_point-name',
    'contact_point-title',
    'contact_point-address',
    'contact_point-email',
    'contact_point-website',
    'contact_point-phone',
    'creator',
    'description',
    'has_policy',
    'identifier',
    'is_reference_by',
    'keyword',
    'landing_page',
    'license',
    'resource_language',
    'relation',
    'rights',
    'qualified_relation',
    'publisher',
    'release_date',
    'theme',
    'title',
    'type',
    'modification_date',
    'qualified_attribution',
    'endpoint_description',
    'endpoint_url',
    'serves_dataset',
  ];

  /**
   * The list of DateTime fields in the form.
   *
   * @var string[]
   */
  protected const DATETIME_FIELDS = [
    'release_date',
    'modification_date',
  ];

  /**
   * A list of multivalued select fields. These values need a special treatment
   * before sending to CKAN.
   *
   * @var string[]
   */
  protected const MULTIVALUED_SELECT_FIELDS = [
    'theme',
    'resource_language',
    'has_policy',
  ];

  /**
   * The dateformat used by datasets.
   */
  protected const DATASET_DATE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DataServiceBaseForm
  {
    /** @var DCATRepositoryInterface $datasetRepository */
    $datasetRepository = $container->get('dexes_dcat.dataservice_repository');

    /** @var APIClientInterface $APIClient */
    $APIClient = $container->get('xs_api_client.api_client_default');

    /** @var ListClientInterface $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');

    /** @var UserStorageInterface $userStorage */
    $userStorage = $entityTypeManager->getStorage('user');

    /** @var DataspaceConfigurationService $dataspaceConfigurationService */
    $dataspaceConfigurationService = $container->get('dexes_dataspace.configuration_service');

    // @phpstan-ignore-next-line
    return new static(
      $datasetRepository,
      $listClient,
      $APIClient,
      $userStorage,
      $dataspaceConfigurationService
    );
  }

  /**
   * DataServiceBaseForm Constructor.
   *
   * @param DCATRepositoryInterface       $dataServiceRepository         The dataset repository to load DataServices from cache
   * @param ListClientInterface           $listClient                    The list client to retrieve lists (for select fields)
   * @param APIClientInterface            $APIClient                     The client to communicate with the Catalog API
   * @param UserStorageInterface          $userStorage                   The user storage
   * @param DataspaceConfigurationService $dataspaceConfigurationService The service that holds the dataspace configuration
   */
  public function __construct(DCATRepositoryInterface $dataServiceRepository,
                              ListClientInterface $listClient,
                              APIClientInterface $APIClient,
                              UserStorageInterface $userStorage,
                              protected DataspaceConfigurationService $dataspaceConfigurationService)
  {
    parent::__construct($dataServiceRepository, $listClient, $APIClient, $userStorage, $dataspaceConfigurationService);
  }

  /**
   * Checks whether a given name/id exists in the Catalog API.
   *
   * @param string $value The name/id
   *
   * @return bool A boolean indicating whether the given name/id exists in the Catalog API
   */
  public function exists(string $value): bool
  {
    try {
      return empty($this->APIClient->contentType('dexes-dataservices')->get($value)->getObject());
    } catch (BaseApiException|BaseClientException|ClientException) {
      return FALSE;
    }
  }

  /**
   * Build the extra part of the form.
   *
   * @param array<string, mixed> $dataService
   *                                          The optional dataservice when editing a dataservice
   *
   * @return array<string, array>
   *                              The extra part of the form
   */
  public function buildExtraPartOfForm($dataService): array
  {
    $form['extra'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Extra'),
      '#open'        => FALSE,
    ];

    $form['extra']['access_rights'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Access'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:Openbaarheidsniveau')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('access_rights', $dataService)
        ? $dataService['access_rights'] : NULL,
    ];

    $form['extra']['creator'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Creator'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Organization')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('creator', $dataService)
        ? $dataService['creator'] : NULL,
    ];

    $form['extra']['landing_page'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Landing page'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('url', $dataService)
        ? $dataService['landing_page'] : NULL,
    ];

    $form['extra']['resource_language'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Language dataset'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Language')),
      '#required'      => TRUE,
      '#multiple'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('resource_language', $dataService)
        ? $dataService['resource_language'] : NULL,
    ];

    $form['extra']['rights'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Rights'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:Openbaarheidsniveau')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('rights', $dataService)
        ? $dataService['rights'] : NULL,
    ];

    $form['extra']['publisher'] = [
      '#type'          => 'select2',
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Organization')),
      '#title'         => $this->t('Publishing organization'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('publisher', $dataService)
        ? $dataService['publisher'] : NULL,
    ];

    $form['extra']['release_date'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Release Date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('release_date', $dataService)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataService['release_date'])
        : NULL,
    ];

    $form['extra']['modification_date'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t('Modification Date'),
      '#attributes'    => [
        'class' => ['form-row'],
      ],
      '#default_value' => array_key_exists('modification_date', $dataService)
        ? DrupalDateTime::createFromFormat(self::DATASET_DATE_FORMAT, $dataService['modification_date'])
        : NULL,
    ];

    $form['extra']['endpoint_description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Endpoint Description'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('endpoint_description', $dataService)
        ? $dataService['endpoint_description'] : NULL,
    ];

    $form['extra']['endpoint_url'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Endpoint URL'),
      '#placeholder'   => 'https://',
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('endpoint_url', $dataService)
        ? $dataService['endpoint_url'] : NULL,
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons for the policy. Selects and returns the fieldset with the names in it.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   *
   * @return array<string, mixed> The modified $form
   */
  public function alterMoreCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['reuse']['license_and_conditions']['has_policy'];
  }

  /**
   * Builds the dataservice form.
   *
   * @param array<string, mixed> $dataService
   *                                          The optional dataset when editing an existing dataset
   *
   * @return array<string, mixed>
   *                              The form definition
   */
  protected function buildDataServiceForm(FormStateInterface $form_state, array $dataService = [], string $title = ''): array
  {
    return $this->buildTitlePartOfForm($title)
      + $this->buildDescriptionPartOfForm($dataService)
      + $this->buildOwnerPartOfForm($dataService)
      + $this->buildExtraPartOfForm($dataService)
      + $this->buildLicensePartOfForm($dataService, $form_state)
      + $this->buildRelationPartOfForm($dataService);
  }

  /**
   * Builds the owner part of the dataset form.
   *
   * @param array<string, mixed> $dataservice
   *                                          The optional dataset when editing an existing dataset
   *
   * @return array<string, array>
   *                              The definition of the owner part of the dataset form
   */
  private function buildOwnerPartOfForm(array $dataservice = []): array
  {
    $form['owner'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Owner'),
      '#description' => $this->t('Information about the owner.'),
      '#open'        => FALSE,
    ];

    $form['owner']['contact'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Contact point'),
      '#description' => $this->t('At least one of e-mail, website or phone is required.'),
    ];

    if (!array_key_exists('contact_point', $dataservice)) {
      $dataservice['contact_point'] = [];
    }

    $form['owner']['contact']['contact_point-name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Owner Name'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('name', $dataservice['contact_point'])
        ? $dataservice['contact_point']['name'] : NULL,
    ];

    $form['owner']['contact']['contact_point-email'] = [
      '#type'          => 'email',
      '#title'         => $this->t('E-mail'),
      '#attributes'    => [
        'name'  => 'contact_point-email',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point-website"]' => ['filled' => FALSE],
          ':input[name="contact_point-phone"]'   => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('email', $dataservice['contact_point'])
        ? $dataservice['contact_point']['email'] : NULL,
    ];

    $form['owner']['contact']['contact_point-website'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Website'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'name'  => 'contact_point-website',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point-email"]' => ['filled' => FALSE],
          ':input[name="contact_point-phone"]' => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('website', $dataservice['contact_point'])
        ? $dataservice['contact_point']['website'] : NULL,
    ];

    $form['owner']['contact']['contact_point-phone'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Phone number'),
      '#attributes'    => [
        'name'  => 'contact_point-phone',
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="contact_point-email"]'   => ['filled' => FALSE],
          ':input[name="contact_point-website"]' => ['filled' => FALSE],
        ],
      ],
      '#default_value' => array_key_exists('phone', $dataservice['contact_point'])
        ? $dataservice['contact_point']['phone'] : NULL,
    ];

    return $form;
  }

  /**
   * Build the description part of the form.
   *
   * @param array<string, mixed> $dataService
   *                                          The optional dataservice when editing a dataservice
   *
   * @return array<string, array>
   *                              The description part of the form
   */
  private function buildDescriptionPartOfForm(array $dataService = []): array
  {
    $form['description'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Description'),
      '#description' => $this->t('A list of fields that describe the dataservice.'),
      '#open'        => TRUE,
    ];

    $form['description']['title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Title'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('title', $dataService)
        ? $dataService['title'] : NULL,
    ];

    $form['description']['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('description', $dataService)
        ? $dataService['description'] : NULL,
    ];

    $form['description']['name'] = [
      '#type'          => 'machine_name',
      '#title'         => $this->t('Machine name'),
      '#machine_name'  => [
        'source'          => ['description', 'title'],
        'replace_pattern' => self::REPLACE_PATTERN,
        'replace'         => self::REPLACE_TOKEN,
        'exists'          => [$this, 'exists'],
      ],
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('name', $dataService)
        ? $dataService['name'] : NULL,
      '#disabled'      => array_key_exists('name', $dataService),
    ];

    $form['description']['theme'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Themes'),
      '#multiple'      => TRUE,
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('Overheid:TaxonomieBeleidsagenda')),
      '#default_value' => array_key_exists('theme', $dataService)
        ? $dataService['theme'] : NULL,
    ];

    return $form;
  }

  /**
   * Build the license part of the form.
   *
   * @param array<string, mixed> $dataService
   *                                          The optional dataservice when editing a dataservice
   *
   * @return array<string, array>
   *                              The license part of the form
   */
  private function buildLicensePartOfForm(array $dataService, FormStateInterface $form_state): array
  {
    $form['reuse']['license_and_conditions'] = [
      '#type'        => 'details',
      '#title'       => $this->t('License and conditions'),
      '#open'        => FALSE,
    ];

    $form['reuse']['license_and_conditions']['license'] = [
      '#type'          => 'select',
      '#title'         => $this->t('License'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:License')),
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('license', $dataService)
        ? $dataService['license'] : NULL,
    ];

    $this->buildPolicyPartOfForm(
      $form_state,
      $form,
      $form['reuse']['license_and_conditions'],
      $this->getSelectedPolicies($form_state, $dataService)
    );

    return $form;
  }

  /**
   * Build the relation part of the form.
   *
   * @param array<string, mixed> $dataService
   *                                          The optional dataservice when editing a dataservice
   *
   * @return array<string, array>
   *                              The relation part of the form
   */
  private function buildRelationPartOfForm(array $dataService): array
  {
    $form['relation']['identifiers'] = [
      '#type'  => 'details',
      '#title' => $this->t('Identifiers and Relations'),
      '#open'  => FALSE,
    ];

    $form['relation']['identifiers']['identifier'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Identifier'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#disabled'      => array_key_exists('identifier', $dataService),
      '#default_value' => array_key_exists('identifier', $dataService)
        ? $dataService['identifier'] : NULL,
    ];

    $form['relation']['identifiers']['conforms_to'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Standards'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('conforms_to', $dataService)
        ? $dataService['conforms_to'] : NULL,
    ];

    $form['relation']['identifiers']['is_referenced_by'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Is Referenced By'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('is_referenced_by', $dataService)
        ? $dataService['is_referenced_by'] : NULL,
    ];

    $form['relation']['identifiers']['qualified_relation'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Qualified Relation'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('qualified_relation', $dataService)
        ? $dataService['qualified_relation'] : NULL,
    ];

    $form['relation']['identifiers']['qualified_attribution'] = [
      '#type'          => 'select2',
      '#title'         => $this->t('Qualified Attribution'),
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Organization')),
      '#default_value' => array_key_exists('qualified_attribution', $dataService)
        ? $dataService['qualified_attribution'] : NULL,
    ];

    $form['relation']['identifiers']['serves_dataset'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Serves Dataset'),
      '#placeholder'   => 'https://',
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#default_value' => array_key_exists('serves_dataset', $dataService)
        ? $dataService['serves_dataset'] : NULL,
    ];

    return $form;
  }
}
