<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataservice;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DeleteDataServiceForm.
 */
class DeleteDataServiceForm extends DataServiceBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'dexes_dcat_delete_dataservice_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
                            ?array $dataservice = NULL): array
  {
    if (empty($dataservice)) {
      throw new NotFoundHttpException();
    }

    $form['#attributes'] = [
      'class' => [
        'd-flex',
        'flex-column',
        'align-items-center',
      ],
    ];

    $form['Are_you_sure'] = [
      '#prefix' => '<h1 class="h5 font-weight-bold">',
      '#suffix' => '</h1>',
      '#markup' => $this->t('Are you sure you wish to delete this dataservice?'),
    ];

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
          'mt-4',
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type'          => 'submit',
      '#value'         => $this->t('Delete dataservice'),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_dcat.dataservice.view', [
        'dataservice' => $dataservice['name'],
      ]),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $dataservice = $this->getRouteMatch()->getParameter('dataservice');

    if (empty($dataservice)) {
      throw new NotFoundHttpException();
    }

    try {
      $response = $this->APIClient->contentType('dexes-dataservices')->delete($dataservice['id']);

      if (FALSE === $response) {
        $this->messenger()
          ->addError(t('A system error prevented the deletion of the DataService.'));
        $form_state->setRebuild();

        return;
      }

      $this->repository->removeDCATItemFromCache($dataservice['name']);

      $form_state->setRedirectUrl(Url::fromRoute('dexes_search.search.dataservice', [
        'scope'       => 'dataservice',
        'query'       => '-',
        'filters'     => '-',
        'page_number' => 1,
      ]));
    } catch (BaseClientException|BaseApiException|ClientException) {
      $this->messenger()
        ->addError(t('A system error prevented the deletion of the DataService.'));
      $form_state->setRebuild();

      return;
    }
  }
}
