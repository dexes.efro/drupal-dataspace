<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form\Dataservice;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;
use XpertSelect\ApiClient\Payload;

/**
 * Class AddDataServiceForm.
 */
class AddDataServiceForm extends DataServiceBaseForm
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dcat_add_dataservice_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form = $this->buildDataServiceForm($form_state, title: $this->t('Add a dataservice'));

    $form['actions'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $form['submit'] = [
      '#type'          => 'submit',
      '#value'         => $this->t('Add dataservice'),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-primary',
          'mb-3',
          'ml-0',
        ],
      ],
    ];

    $form['cancel'] = [
      '#type'  => 'link',
      '#title' => $this->t('Cancel'),
      '#url'   => Url::fromRoute('dexes_search.search.dataservice', [
        'scope'       => 'dataservice',
        'query'       => '-',
        'filters'     => '-',
        'page_number' => 1,
      ]),
      '#attributes'    => [
        'class' => [
          'btn',
          'btn-danger',
          'mb-3',
          'ml-3',
          'text-light',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $newDataService = $this->getFieldsFromForm($form_state,
      self::FIELDS,
      self::DATETIME_FIELDS,
      self::MULTIVALUED_SELECT_FIELDS);

    $identifierPrefix = $this->getIdentifierPrefix();

    $newDataService['identifier'] = array_key_exists('identifier', $newDataService)
      ? $newDataService['identifier']
      : Url::fromUri($identifierPrefix . $newDataService['name'])
        ->toString();

    $newDataService['dataspace_uri'] = $this->dataspaceConfigurationService->get('uri');
    $this->normalizePolicies($newDataService);

    try {
      $payload = new Payload();
      $payload->addValues($newDataService);
      $response = $this->APIClient->contentType('dexes-dataservices')->store($payload)->getobject();

      $form_state->setRedirectUrl(Url::fromRoute(
        'dexes_dcat.dataservice.view', [
          'dataservice' => $newDataService['name'],
        ]
      ));
    } catch (BaseClientException|BaseApiException|ClientException) {
      $this->messenger()
        ->addError(t('A system error prevented the creation of the DataService.'));
      $form_state->setRebuild();

      return;
    }
  }

  /**
   * Get the identifier prefix. If no identifier_prefix value for datasets is set it will return the dataspace uri.
   */
  private function getIdentifierPrefix(): string
  {
    $prefix = $this->dataspaceConfigurationService->get('uri');

    if (!str_ends_with($prefix, '/')) {
      $prefix .= '/';
    }

    return $prefix . 'dataservice/';
  }
}
