<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Form;

use DateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Drupal\xs_lists\ListClientInterface;
use XpertSelect\ApiClient\APIClientInterface;

/**
 * Class DCATFormBase.
 */
abstract class DCATFormBase extends FormBase
{
  /**
   * The acceptable characters to allow in a machine_name.
   *
   * @var string
   */
  public const REPLACE_PATTERN = '[^a-z0-9-]+';

  /**
   * All unacceptable characters should be replaced with this token.
   *
   * @var string
   */
  public const REPLACE_TOKEN = '-';

  /**
   * DCATFormBase Constructor.
   *
   * @param DCATRepositoryInterface       $repository                    the DCATRepository for interacting with cacheable DCAT items
   * @param ListClientInterface           $listClient                    The API Client for interacting with the Catalog API
   * @param APIClientInterface            $APIClient                     List client for retrieving lists (for select fields)
   * @param UserStorageInterface          $userStorage                   User storage for retrieving Drupal users
   * @param DataspaceConfigurationService $dataspaceConfigurationService The service that has the dataspace config
   */
  public function __construct(protected DCATRepositoryInterface $repository,
                              protected ListClientInterface $listClient,
                              protected APIClientInterface $APIClient,
                              protected UserStorageInterface $userStorage,
                              protected DataspaceConfigurationService $dataspaceConfigurationService)
  {
  }

  /**
   * Submit handler for the "add" button. Increments the max counter and causes a rebuild.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   */
  public function addOne(array &$form, FormStateInterface $form_state): void
  {
    $form_state->set('policies.count', $form_state->get('policies.count') + 1);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "delete" button. Decrements the max counter, removes the value and causes a rebuild.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   */
  public function deleteOne(array &$form, FormStateInterface $form_state): void
  {
    $userInput = $form_state->getUserInput();
    $dataIndex = $userInput['delete_index'];
    if (isset($userInput['has_policy'][$dataIndex])) {
      unset($userInput['has_policy'][$dataIndex]);
    }
    $form_state->setUserInput($userInput);

    $form_state->set('policies.count', $form_state->get('policies.count') - 1);
    $form_state->setRebuild();
  }

  /**
   * Gets the form values given a list of fields and the form state.
   *
   * @param FormStateInterface $form_state
   *                                                      The current state of the form containing the POSTed values
   * @param array<int, string> $fields
   *                                                      The list of fields to get the values for
   * @param array<int, string> $datetime_fields
   *                                                      The list of datetime field values
   * @param array<int, string> $multivalued_select_fields
   *                                                      The list of multivalued select fields
   *
   * @return array<string, mixed>
   *                              The list of field values ($field => $value)
   */
  protected function getFieldsFromForm(FormStateInterface $form_state,
                                       array $fields,
                                       array $datetime_fields = [],
                                       array $multivalued_select_fields = []): array
  {
    $package = [];

    foreach ($fields as $field) {
      $value = $form_state->getValue($field);

      if (empty($value)) {
        continue;
      }

      if (in_array($field, $datetime_fields)) {
        /** @var DateTime $value */
        $value = $value->format('Y-m-d\TH:i:s');
      } elseif (in_array($field, $multivalued_select_fields)) {
        $value = array_values($value);
      }

      if (str_contains($field, '-')) {
        $split                         = explode('-', $field);
        $package[$split[0]][$split[1]] = $value;
      } else {
        $package[$field] = $value;
      }
    }

    return $package;
  }

  /**
   * Filters the list of organizations based on the organizations the user is
   * member of.
   *
   * @return array<string, string> The organizations the user is member of or all organizations in case the
   *                               user has the manage all datasets permission
   */
  protected function filterOrganizationsByUser(): array
  {
    /** @var UserInterface $user */
    $user = $this->userStorage->load($this->currentUser()->id());

    $organizations = $this->listClient->formatter()
      ->flatten($this->listClient->list('DONL:Organization'));

    if ($user->hasPermission('manage all datasets')) {
      return $organizations;
    }

    return [];
  }

  /**
   * Adds a title to the form.
   *
   * @param string $title The title of the form
   *
   * @return array<string, mixed> The title in a form array
   */
  protected function buildTitlePartOfForm(string $title): array
  {
    if (empty($title)) {
      return [];
    }

    $form['title'] = [
      '#prefix' => '<h1 class="h2 font-weight-bold">',
      '#suffix' => '</h1>',
      '#markup' => $title,
    ];

    return $form;
  }

  /**
   * Adds a policy field to the form.
   */
  protected function buildPolicyPartOfForm(FormStateInterface $form_state, array &$form, array &$formPart, array $selectedPolicies = []): void
  {
    $policies = $this->getPolicyOptions();
    $count    = $form_state->get('policies.count');
    $lists    = [];

    foreach ($selectedPolicies as $key => $selectedPolicy) {
      if (is_array($selectedPolicy)) {
        $selectedPolicy = $selectedPolicy['policy_' . $key];
      }
      if (isset($policies[$selectedPolicy])) {
        $lists[] = $selectedPolicy;
      }
    }

    if (NULL === $count) {
      $count = count(array_keys($lists));
      $form_state->set('policies.count', $count);
    }

    $formPart['has_policy'] = [
      '#type'   => 'container',
      '#tree'   => TRUE,
      '#prefix' => '
        <div id="policies-wrapper">
          <table>
            <thead>
            <tr>
              <th class="font-weight-normal pb-2">Policy</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
      ',
      '#suffix' => '
            </tbody>
          </table>
        </div>
      ',
    ];

    for ($i = 0; $i < $count; ++$i) {
      $formPart['has_policy'][$i] = [
        'policy_' . $i => [
          '#type'          => 'select2',
          '#title'         => $this->t('Type')->render(),
          '#title_display' => 'invisible',
          '#default_value' => $lists[$i] ?? NULL,
          '#options'       => $policies,
          '#prefix'        => '<tr><td>',
          '#suffix'        => '</td>',
          '#empty_option'  => $this->t('- Select -'),
          '#attributes'    => [
            'class' => ['w-100', 'policies-multi-select'],
            'id'    => 'policy-select-' . $i,
          ],
        ],
        'static_policy_' . $i => [
          '#prefix'     => '<td class="d-none static-policy-cell"><p>' . ($lists[$i] ?? ''),
          '#suffix'     => '</p></td>',
        ],
        'delete_' . $i => [
          '#type'                    => 'submit',
          '#value'                   => $this->t('Delete')->render(),
          '#submit'                  => ['::deleteOne'],
          '#prefix'                  => '<td>',
          '#suffix'                  => '</td></tr>',
          '#limit_validation_errors' => [],
          '#attributes'              => [
            'class'             => ['btn', 'btn-danger', 'ml-2', 'delete-button', 'mb-1'],
            'data-delete-index' => $i,
          ],
          '#data'                    => $i,
          '#ajax'                    => [
            'callback' => '::alterMoreCallback',
            'wrapper'  => 'policies-wrapper',
            'event'    => 'click',
          ],
        ],
      ];
    }

    if (0 === $count) {
      $formPart['has_policy']['message'] = [
        '#type'       => 'container',
        '#prefix'     => '<tr><td class="font-italic">' . t('No policies are applicable'),
        '#suffix'     => '</td></tr>',
      ];
    }

    $formPart['row']['delete_index'] = [
      '#type'       => 'hidden',
      '#attributes' => [
        'id' => 'delete_index',
      ],
    ];

    $formPart['row']['#type'] = 'actions';
    $formPart['row']['add']   = [
      '#type'                    => 'submit',
      '#value'                   => $this->t('Add'),
      '#submit'                  => ['::addOne'],
      '#limit_validation_errors' => [],
      '#attributes'              => [
        'class' => ['btn', 'btn-success', 'add-button', 'ml-0'],
      ],
      '#ajax'                    => [
        'callback' => '::alterMoreCallback',
        'wrapper'  => 'policies-wrapper',
        'event'    => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'dexes_dcat/multi-select';
  }

  /**
   * Gets the selected policies from the correct source.
   *
   * @param FormStateInterface $form_state The current state of the form
   * @param array              $data       The saved data
   *
   * @return array|string[] The selected policies
   */
  protected function getSelectedPolicies(FormStateInterface $form_state, array $data): array
  {
    $userData         = $form_state->getUserInput();
    $selectedPolicies = $userData['has_policy'] ??
      (array_key_exists('has_policy', $data) ? $data['has_policy'] : []);
    if (is_string($selectedPolicies)) {
      $selectedPolicies = [$selectedPolicies];
    }

    return $selectedPolicies;
  }

  /**
   * Normalizes the form values for policies.
   *
   * @param array $formData The form data
   */
  protected function normalizePolicies(array &$formData): void
  {
    $policies = [];
    if (isset($formData['has_policy'])) {
      foreach ($formData['has_policy'] as $index => $policy) {
        $policies[] = $policy['policy_' . $index];
      }
    }
    $formData['has_policy'] = $policies;
  }

  /**
   * Formats the policy list data to key value pairs for a form.
   */
  private function getPolicyOptions(): array
  {
    $list    = $this->listClient->list('DEXES:Policies');
    $data    = $list->hasData() ? $list->getData() : [];
    $options = [];

    foreach ($data as $policyKey => $policy) {
      $options[$policyKey] = $policy['policy_info']['policy_label'][$list->getLanguageCode()];
    }

    return $options;
  }
}
