/* globals jQuery, Drupal */

(function ($, Drupal) {
  let data = [];

  Drupal.behaviors.multiSelect = {
    attach: function () {
      "use strict";

      const selects = $(".policies-multi-select");
      data.forEach((value, index) => {
        $(selects[index]).val(value);
      });

      selects.each(() => {
        selects.off("change", updateDisabledValues).on("change", updateDisabledValues);
      });
      updateDisabledValues();

      $(".delete-button").off("mousedown").on("mousedown", (event) => {
        const deleteIndex = event.currentTarget.dataset.deleteIndex;
        document.getElementById("delete_index").value = deleteIndex;
        data = [];
        selects.each((index, element) => {
          if (deleteIndex !== "" + index) {
            data.push($(element).val());
          }
        });
      });

      $(".add-button").off("mousedown").on("mousedown", () => {
        data = [];
        selects.each((index, element) => {
          data.push($(element).val());
        });
      });

      function updateDisabledValues(event) {
        selects.find(":not([value]):disabled").prop("disabled", false);
        selects.each((index, element) => {
          const jElement = $(element);
          selects.each((innerIndex, innerElement) => {
            if (innerIndex !== index) {
              jElement.find("option[value='" + $(innerElement).val() + "']").prop("disabled", true);
            }
          });

          const cell = jElement.closest("td");
          if (!(jElement.val() === undefined ||jElement.val() === null || jElement.val() === "") && !cell.hasClass("d-none")) {
            cell.addClass("d-none");
            cell.next().removeClass("d-none")
              .find("p").text(jElement.find("option:selected").text());
          }
        });
      }
    }
  };
})(jQuery, Drupal);
