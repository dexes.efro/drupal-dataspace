<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use RuntimeException;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DataspaceConfigurationService.
 *
 * Drupal service for interacting with the Dataspace configuration data.
 */
final class DataspaceConfigurationService
{
  /**
   * The cache key holding the dataspace configuration.
   *
   * @var string
   */
  public const CACHE_KEY = 'dexes_dataspace.configuration';

  /**
   * The filter field delimiter of the Catalog API.
   */
  private const FILTER_FIELD_DELIMITER = ';';

  /**
   * The in-memory cache of the dataspace configuration to prevent multiple requests for the dataspace configuration to
   * result in multiple cache lookups.
   *
   * @var null|array<string, mixed>
   */
  private static ?array $REQUEST_CACHE = NULL;

  /**
   * DataspaceConfigurationService constructor.
   *
   * @param CacheBackendInterface    $cacheService    The Drupal caching service
   * @param APIClientInterface       $apiClient       The client for communicating with the catalog API
   * @param LanguageManagerInterface $languageManager The Drupal language manager
   */
  public function __construct(private CacheBackendInterface $cacheService, private APIClientInterface $apiClient,
                              private LanguageManagerInterface $languageManager)
  {
  }

  /**
   * Retrieve the dataspace configuration from the cache. If it is currently not available in the cache, it will be
   * retrieved from the Dexes Catalog API.
   *
   * @return array<string, mixed> The dataspace configuration
   */
  public function getAll(): array
  {
    if (NULL !== self::$REQUEST_CACHE) {
      return self::$REQUEST_CACHE;
    }

    $cachedConfiguration = $this->cacheService->get(self::CACHE_KEY, TRUE);

    if (!empty($cachedConfiguration->data)) {
      return (array) $cachedConfiguration->data;
    }

    return $this->loadConfiguration();
  }

  /**
   * Retrieve the value for a specific key in the dataspace configuration.
   *
   * @param string $key The key to look for
   *
   * @return mixed The value of the key
   */
  public function get(string $key): mixed
  {
    $configuration = $this->getAll();

    if (!array_key_exists($key, $configuration)) {
      return NULL;
    }

    return $configuration[$key];
  }

  /**
   * Load the dataspace configuration from the Catalog API, cache it permanently and return it.
   *
   * @return array<string, mixed> The dataspace configuration
   */
  private function loadConfiguration(): array
  {
    $uri = $this->getDataspaceUri();

    try {
      $filterResults = $this->apiClient
        ->contentType('dexes-dataspaces')
        ->all(filters: ['uri' . self::FILTER_FIELD_DELIMITER . $uri]);

      if (0 === count($filterResults)) {
        throw new RuntimeException(sprintf(
          'Dataspace "%s" could not be resolved to an existing dataspace in the Catalog API',
          $uri
        ));
      }

      $configuration = $filterResults[0]->getObject();

      $configuration['ruleframework'] = $this->apiClient
        ->contentType('dexes-ruleframeworks')
        ->get(strval($configuration['ruleframework_id']))
        ->getObject();

      self::$REQUEST_CACHE = $configuration;
      $this->cacheService->set(self::CACHE_KEY, $configuration);

      return $configuration;
    } catch (BaseApiException|BaseClientException) {
      throw new RuntimeException('Dataspace configuration could not be retrieved from the Catalog API');
    }
  }

  /**
   * Determine and return the dataspace URI of the current Drupal instance.
   *
   * @return string The current dataspace URI
   */
  private function getDataspaceUri(): string
  {
    $noLanguage = $this->languageManager->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE);

    $uri = Url::fromRoute('<front>', [], ['absolute' => TRUE, 'language' => $noLanguage])->toString();

    if ($uri instanceof GeneratedUrl) {
      $uri = $uri->getGeneratedUrl();
    }

    $uri = str_replace(['http://', '.staging', '.local'], ['https://', '', '.nl'], $uri);

    if (str_ends_with($uri, '/')) {
      $uri = substr($uri, 0, strlen($uri) - 1);
    }

    return $uri;
  }
}
