<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Search;

use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class DataspaceBaseProfile.
 *
 * Base search profile that ensures the proper filters are set for all queries against the search-engine.
 */
abstract class DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * DataspaceBaseProfile constructor.
   *
   * @param DataspaceConfigurationService $dataspaceConfigurationService The dataspace configuration service
   */
  final public function __construct(private DataspaceConfigurationService $dataspaceConfigurationService)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [sprintf('relation_dataspace:"%s"', $this->dataspaceConfigurationService->get('uri'))];
  }
}
