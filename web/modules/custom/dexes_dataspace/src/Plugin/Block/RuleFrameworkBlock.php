<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Ruleframework' Block.
 *
 * @Block(
 *   id = "dexes_ruleframework_block",
 *   admin_label = @Translation("Dexes RuleFramework Block"),
 * )
 */
final class RuleFrameworkBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition): self
  {
    /** @var DataspaceConfigurationService $dataspaceConfigurationService */
    $dataspaceConfigurationService = $container->get('dexes_dataspace.configuration_service');

    return new self($configuration, $plugin_id, $plugin_definition, $dataspaceConfigurationService);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              private DataspaceConfigurationService $dataspaceConfigurationService)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme'         => 'ruleframework',
      '#ruleframework' => $this->dataspaceConfigurationService->get('ruleframework'),
    ];
  }
}
