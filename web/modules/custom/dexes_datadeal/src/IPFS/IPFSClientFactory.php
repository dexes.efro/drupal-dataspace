<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\IPFS;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class IPFSClientFactory.
 *
 * Responsible for creating implementations of the IPFSClientInterface.
 */
class IPFSClientFactory
{
  /**
   * Creates a IPFSClient based on the given input.
   *
   * @param LoggerChannelFactoryInterface $loggerFactory
   *                                                     The factory for creating the appropriate logging channel
   * @param ClientFactory                 $clientFactory
   *                                                     The factory for creating the Guzzle client
   * @param ConfigFactoryInterface        $configFactory
   *                                                     The factory for loading the dexes_datadeal settings
   * @param CacheBackendInterface         $cacheBackend
   *                                                     The caching backend to assign
   *
   * @return IPFSClientInterface
   *                             The created instance
   */
  public static function create(LoggerChannelFactoryInterface $loggerFactory,
                                ClientFactory $clientFactory,
                                ConfigFactoryInterface $configFactory,
                                CacheBackendInterface $cacheBackend): IPFSClientInterface
  {
    $settings = $configFactory->get('dexes_datadeal.settings');

    return new IPFSClient(
      $loggerFactory->get(IPFSClientInterface::LOG_CHANNEL),
      $clientFactory->fromOptions(),
      $cacheBackend,
      (string) $settings->get('ipfs_endpoint'),
      $settings->get('cache_duration') ?? 0
    );
  }
}
