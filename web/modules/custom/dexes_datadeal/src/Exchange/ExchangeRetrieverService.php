<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Exchange;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_solr\Search\Response\SearchDocumentInterface;
use Drupal\xs_solr\Search\Response\SolrSearchResponseInterface;
use Drupal\xs_solr\SolrClientInterface;

/**
 * Class ExchangeRetrieverService.
 *
 * Basic implementation of the ExchangeRetrieverServiceInterface, which
 * uses Solr to retrieve "exchanges": deals and offers.
 */
class ExchangeRetrieverService implements ExchangeRetrieverServiceInterface
{
  /**
   * ExchangeRetrieverService constructor.
   *
   * @param SolrClientInterface    $solr_client     The Solr client to use
   * @param string                 $solr_collection the Solr collection to query for data deals and offers
   * @param LoggerChannelInterface $logger          The logger to use
   */
  public function __construct(private SolrClientInterface $solr_client,
                              private string $solr_collection,
                              private LoggerChannelInterface $logger)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getDatadealsForDataset(array $dataset, int $start = 0, int $rows = 1): array
  {
    return $this->getExchangeForDataset($dataset, 'deal', $start, $rows);
  }

  /**
   * {@inheritdoc}
   */
  public function getOffersForDataset(array $dataset, int $start = 0, int $rows = 1): array
  {
    return $this->getExchangeForDataset($dataset, 'offer', $start, $rows);
  }

  /**
   * Given a dataset query the Solr Exchange for the given type.
   *
   * @param array  $dataset The dataset for which we return exchange types
   * @param string $type    The exchange type to return
   * @param int    $start   The start index of the result of the response of Solr
   *                        this method returns
   * @param int    $rows    The maximum number of results the method returns
   *
   * @return array<string, mixed> The result
   */
  private function getExchangeForDataset(array $dataset, string $type, int $start, int $rows): array
  {
    $solrResponse = $this->getResponseFromSolr($dataset['resources'], $type, $start, $rows);
    $documents    = [];
    $numFound     = 0;

    if (NULL !== $solrResponse) {
      $documents = $this->getExchangeJsonForDataset($solrResponse->getDocuments());
      $numFound  = $solrResponse->getNumFound();
    }

    return [
      'documents' => $documents,
      'numFound'  => $numFound,
    ];
  }

  /**
   * Map the exchange_json field in the Solr documents.
   *
   * @param array $documents an array of Solr Documents
   *
   * @return array<int|string, mixed> The exchange object
   */
  private function getExchangeJsonForDataset(array $documents): array
  {
    return array_map(function(SearchDocumentInterface $exchange) {
      $exchange_json = $exchange->getStringField('exchange_json');

      if (NULL === $exchange_json) {
        $this->logger->error('Exchange JSON NULL (@id)', [
          '@id' => $exchange->getStringField('id'),
        ]);

        return;
      }

      $exchange_array = json_decode($exchange_json, TRUE);

      if (json_last_error() !== JSON_ERROR_NONE) {
        $this->logger->error('Exchange JSON error (@id); @exchange', [
          '@id'       => $exchange->getStringField('id'),
          '@exchange' => $exchange_json,
        ]);

        return;
      }

      return $exchange_array;
    }, $documents);
  }

  /**
   * Queries Solr and returns the resulting response.
   *
   * @param array  $resources the resources parameters to query for
   * @param string $type      The exchange type to filter on
   * @param int    $start     The start index of the result set of the response of Solr
   *                          which this method returns
   * @param int    $rows      The maximum number of results this method returns
   *
   * @return ?SolrSearchResponseInterface The Solr Search Response
   */
  private function getResponseFromSolr(array $resources, string $type, int $start, int $rows): ?SolrSearchResponseInterface
  {
    $result = $this->solr_client->collection($this->solr_collection)
      ->query(
        'select',
        $this->createSolrQuery($resources),
        [sprintf('exchange_type:"%s"', $type)],
        $start,
        $rows,
        ['fl' => 'id, exchange_json, created_at', 'sort' => 'created_at desc']
      );

    if (NULL === $result) {
      return NULL;
    }

    return $result->getResponse();
  }

  /**
   * Creates a Solr query, based on a given list of resources.
   *
   * @param array<int, array<string, mixed>> $resources
   *                                                    The list of resources
   *
   * @return string
   *                The Solr query in a string
   */
  private function createSolrQuery(array $resources): string
  {
    $url_queries = array_map(function(array $resource) {
      return sprintf('resource:"%s"', $resource['url']);
    }, $resources);

    return join(' OR ', $url_queries);
  }
}
