<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Exchange;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_solr\SolrClientInterface;

/**
 * Class ExchangeRetrieverServiceFactory.
 *
 * Responsible for creating implementations of the ExchangeRetrieverServiceInterface.
 */
class ExchangeRetrieverServiceFactory
{
  /**
   * Creates an ExchangeRetrieverService based on the given input.
   *
   * @param SolrClientInterface           $solr_client   The Solr client to use for retrieving exchange types
   * @param LoggerChannelFactoryInterface $loggerFactory The factory for creating the logger
   * @param ConfigFactoryInterface        $configFactory The factory for loading the dexes_datadeal settings
   *
   * @return ExchangeRetrieverServiceInterface The created instance
   */
  public static function create(SolrClientInterface $solr_client,
                                LoggerChannelFactoryInterface $loggerFactory,
                                ConfigFactoryInterface $configFactory): ExchangeRetrieverServiceInterface
  {
    $settings = $configFactory->get('dexes_datadeal.settings');

    return new ExchangeRetrieverService(
      $solr_client,
      $settings->get('solr_collection') ?? '',
      $loggerFactory->get(ExchangeRetrieverServiceInterface::LOG_CHANNEL)
    );
  }
}
