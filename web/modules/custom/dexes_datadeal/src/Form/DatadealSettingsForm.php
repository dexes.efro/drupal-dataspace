<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DatadealSettingsForm.
 *
 * Enables a settings form for configuring the dexes_datadeal settings.
 */
class DatadealSettingsForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_datadeal_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $settings = $this->config('dexes_datadeal.settings');

    $form['exchange_retriever_settings'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Datadeal and offer retrieve settings'),
      '#description' => $this->t('Configure the Solr settings to retrieve datadeals and offers.'),
      '#open'        => TRUE,
    ];

    $form['exchange_retriever_settings']['solr_collection'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Solr collection'),
      '#default_value' => $settings->get('solr_collection'),
      '#required'      => TRUE,
    ];

    $form['ipfs'] = [
      '#type'        => 'details',
      '#title'       => $this->t('IPFS Settings'),
      '#description' => $this->t('Configure the IPFS endpoint settings. The IPFS endpoint is used as the authoritative source of the datadeal contents.'),
      '#open'        => TRUE,
    ];

    $form['ipfs']['ipfs_endpoint'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('IPFS Base URL'),
      '#default_value' => $settings->get('ipfs_endpoint'),
      '#placeholder'   => 'https://',
    ];

    $form['ipfs']['cache_duration'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Cache duration in minutes'),
      '#default_value' => $settings->get('cache_duration'),
      '#placeholder'   => '15',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $settings = $this->config('dexes_datadeal.settings');

    $settings->set(
      'solr_collection',
      $form_state->getValue('solr_collection')
    );

    $settings->set(
      'ipfs_endpoint',
      $form_state->getValue('ipfs_endpoint')
    );

    $settings->set(
      'cache_duration',
      $form_state->getValue('cache_duration')
    );

    $settings->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['dexes_datadeal.settings'];
  }
}
