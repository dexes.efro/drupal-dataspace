<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Controller;

use Drupal\dexes_datadeal\Search\SearchDataDealProfile;
use Drupal\dexes_search\Controller\SearchController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchDataDealController.
 *
 * This controller handles all search requests that searches through
 * the "datadeal" external content type.
 */
class SearchDataDealController extends SearchController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SearchDataDealController
  {
    /** @var SearchDatadealProfile $search_profile */
    $search_profile = $container->get('dexes_datadeal.search_profile.datadeal');

    return parent::createController($container, $search_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchableName(): string
  {
    return 'Datadeals';
  }
}
