<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dexes_menu\Services\MenuServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseMenuBlock.
 */
abstract class BaseMenuBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    /** @var MenuServiceInterface $menu_service */
    $menu_service = $container->get('dexes_menu.menu_service');

    // @phpstan-ignore-next-line
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $menu_service
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id,
                                    $plugin_definition,
                              protected MenuServiceInterface $menu_service)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#menu_items_list' => $this->menu_service->getMainMenu(),
      '#sub_items_list'  => $this->menu_service->getSubMenu(),
      '#cache'           => [
        'max-age' => 0,
      ],
    ];
  }
}
