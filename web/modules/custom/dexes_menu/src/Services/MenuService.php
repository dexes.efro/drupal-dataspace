<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Services;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\dexes_dataspace\Service\DataspaceConfigurationService;

/**
 * Class MenuService.
 */
class MenuService implements MenuServiceInterface
{
  use StringTranslationTrait;

  public function __construct(private readonly DataspaceConfigurationService $dataspaceConfigurationService)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getMainMenu(): array
  {
    return [
      'Data'       => $this->getDataMenu(),
      'Dataspaces' => $this->getDataspacesMenu(),
      'My Dexes'   => $this->getMyDexesMenu(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSubMenu(): array
  {
    return [
      'Dexes'   => $this->getDexesMenu(),
      'Support' => $this->getSupportMenu(),
    ];
  }

  /**
   * Get the support menu.
   *
   * @return array[]
   */
  private function getSupportMenu(): array
  {
    return [
      [
        'link'     => '/about',
        'text'     => $this->t('About Dexes'),
        'disabled' => TRUE,
      ],
      [
        'link'     => '/contact',
        'text'     => $this->t('Support'),
        'disabled' => TRUE,
      ],
    ];
  }

  /**
   * Get the Dexes menu.
   *
   * @return array[]
   */
  private function getDexesMenu(): array
  {
    return [
      [
        'link'     => Url::fromRoute('dexes_search.search.all.default'),
        'text'     => $this->t('All data and services'),
        'disabled' => TRUE,
      ],
      [
        'link'     => '/about',
        'text'     => $this->t('Share your data'),
        'disabled' => TRUE,
      ],
      [
        'link'     => '/dataspace',
        'text'     => $this->t('Start a dataspace'),
        'disabled' => TRUE,
      ],
    ];
  }

  /**
   * Get the My Dexes menu.
   *
   * @return array[]
   */
  private function getMyDexesMenu(): array
  {
    return [
      [
        'link' => '/my-account',
        'text' => $this->t('My profile'),
      ],
      [
        'link'     => '/my-account/dataspaces',
        'text'     => $this->t('My dataspaces'),
        'disabled' => TRUE,
      ],
      [
        'link'     => '/my-account/datasets',
        'text'     => $this->t('My shared data'),
        'disabled' => TRUE,
      ],
      [
        'link'     => '/my-account/settings',
        'text'     => $this->t('My support'),
        'disabled' => TRUE,
      ],
    ];
  }

  /**
   * Get the dataspaces menu.
   *
   * @return array[]
   */
  private function getDataspacesMenu(): array
  {
    $ruleframework = $this->dataspaceConfigurationService->get('ruleframework');

    $hasUrl = array_key_exists('url', $ruleframework) && !empty($ruleframework['url']);

    $url               = $hasUrl ? $ruleframework['url'] : '#';
    $ruleframeworkName = $ruleframework['title'] ?? '';

    return [
      [
        'link'     => $url . '/search/all/query/-/filters/type%3Adataspace/page/1',
        'text'     => $this->t('More dataspaces on @ruleframework', ['@ruleframework' => $ruleframeworkName]),
        'disabled' => !$hasUrl,
        'classes'  => 'disabled',
      ],
      [
        'link'     => $url . '/search/all/query/-/filters/-/page/1',
        'text'     => $this->t('Search in dataspaces'),
        'disabled' => !$hasUrl,
        'classes'  => 'disabled',
      ],
      [
        'link'     => $url,
        'text'     => $this->t('Start your own dataspace!'),
        'disabled' => !$hasUrl,
        'classes'  => 'disabled',
      ],
    ];
  }

  /**
   * Get the data menu.
   *
   * @return array[]
   */
  private function getDataMenu(): array
  {
    return [
      [
        'link' => Url::fromRoute('dexes_search.search.all.default'),
        'text' => $this->t('Search data'),
      ],
      [
        'link' => Url::fromRoute('dexes_dcat.dataset.add'),
        'text' => $this->t('Add dataset'),
      ],
      [
        'link' => Url::fromRoute('dexes_dcat.dataservice.add'),
        'text' => $this->t('Add dataservice'),
      ],
    ];
  }
}
