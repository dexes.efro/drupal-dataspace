<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Services;

/**
 * Interface MenuServiceInterface.
 */
interface MenuServiceInterface
{
  /**
   * Get the sub menu.
   */
  public function getSubMenu(): array;

  /**
   * Get the main menu.
   */
  public function getMainMenu(): array;
}
