<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\xs_lists\ListClientInterface;

/**
 * Set dynamic allowed values for the theme field.
 *
 * @param fieldStorageConfig          $definition
 *                                                The field definition
 * @param null|contentEntityInterface $entity
 *                                                The entity being created if applicable
 *
 * @return array
 *               An array of possible key and value options
 *
 * @see options_allowed_values()
 */
function dexes_content_types_get_theme_list(FieldStorageConfig $definition, ?ContentEntityInterface $entity = NULL)
{
  /** @var ListClientInterface $list_client */
  $list_client = Drupal::service('xs_lists.list_client');

  return $list_client->formatter()->flatten($list_client->list('Overheid:TaxonomieBeleidsagenda'));
}
