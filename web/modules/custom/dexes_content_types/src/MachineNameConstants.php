<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types;

/**
 * Class MachineNameConstants.
 *
 * Reusable constants pertaining to the machine_name of content-type Nodes.
 */
class MachineNameConstants
{
  /**
   * The acceptable characters to allow in a machine_name.
   *
   * @var string
   */
  public const REPLACE_PATTERN = '[^a-z0-9-]+';

  /**
   * All unacceptable characters should be replaced with this token.
   *
   * @var string
   */
  public const REPLACE_TOKEN = '-';
}
