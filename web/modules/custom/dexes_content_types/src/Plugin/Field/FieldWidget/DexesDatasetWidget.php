<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dexes_dataset_text' widget.
 *
 * @FieldWidget(
 *   id = "dexes_dataset_text",
 *   module = "dexes_content_types",
 *   label = @Translation("Dexes dataset"),
 *   field_types = {
 *     "dexes_dataset"
 *   }
 * )
 */
class DexesDatasetWidget extends WidgetBase
{
  /**
   * Validate Dexes dataset.
   *
   * @param array<mixed, mixed> $element
   *                                        An array containing the form element for the widget
   * @param FormStateInterface  $form_state
   *                                        The current state of the form
   *
   * TODO: to be implemented
   */
  public static function validate(array $element,
                                  FormStateInterface $form_state): void
  {
    $value = $element['#value'];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta,
                              array $element, array &$form,
                              FormStateInterface $form_state): array
  {
    $value = $items[$delta]->value ?? '';

    $element += [
      '#type'                          => 'textfield',
      '#autocomplete_route_name'       => 'dexes_content_types.autocomplete',
      '#autocomplete_route_parameters' => ['type' => 'dataset'],
      '#default_value'                 => $value,
      '#maxlength'                     => 255,
      '#element_validate'              => [
        [static::class, 'validate'],
      ],
    ];

    return ['value' => $element];
  }
}
