<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'dexes_dataset' field type.
 *
 * @FieldType(
 *   id = "dexes_dataset",
 *   label = @Translation("Dexes dataset"),
 *   module = "dexes_content_types",
 *   description = @Translation("A Dexes dataset field type."),
 *   default_widget = "dexes_dataset_text",
 *   default_formatter = "dexes_dataset_text",
 * )
 */
class DexesDatasetItem extends FieldItemBase
{
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array
  {
    return [
      'columns' => [
        'value' => [
          'type'     => 'text',
          'size'     => 'small',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array
  {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Dexes dataset'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool
  {
    $value = $this->get('value')->getValue();

    return NULL === $value || '' === $value;
  }
}
