<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Controller;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_content_types\Controller\BaseContentTypeJSONExporter;
use Drupal\xs_lists\TranslatableList\TranslatableListFactoryInterface;

/**
 * Class DexesContentTypeJSONExporter.
 */
abstract class DexesContentTypeJSONExporter extends BaseContentTypeJSONExporter
{
  /**
   * {@inheritdoc}
   */
  protected function getNodeIdentifier(EntityInterface $node): string
  {
    if (!$node instanceof ContentEntityInterface) {
      return (string) $node->id();
    }

    return $node->get('xs_identifier')->getValue()[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  protected function formatNodeContents(EntityInterface $node): array
  {
    if (!$node instanceof ContentEntityInterface) {
      return [];
    }

    $representation = ['labels' => []];

    foreach ($node->getTranslationLanguages() as $language) {
      $language_key = $language->getId();

      if (!array_key_exists($language_key, TranslatableListFactoryInterface::LANGUAGE_MAP)) {
        continue;
      }

      $translated_node = $node->getTranslation($language_key);

      if ($translated_node instanceof NodeInterface) {
        $label_key = TranslatableListFactoryInterface::LANGUAGE_MAP[$language_key];

        $representation['labels'][$label_key] = $translated_node->getTitle();
      }
    }

    return $representation;
  }
}
