<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_resolver\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\dexes_resolver\Search\SearchURIProfile;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ResolveController.
 *
 * This controller handles resolve requests. The controller redirects a request with a given URI to its URL in Drupal.
 */
final class ResolveController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchServiceInterface $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchURIProfile $searchProfile */
    $searchProfile = $container->get('dexes_resolver.search_profile.uri');

    return new self($searchService, $searchProfile);
  }

  /**
   * ResolveController constructor.
   *
   * @param SearchServiceInterface $searchService The search interface to use for searching
   * @param SearchProfileInterface $searchProfile The search profile to apply while searching
   */
  public function __construct(private SearchServiceInterface $searchService,
                              private SearchProfileInterface $searchProfile)
  {
  }

  /**
   * Performs a search query to the search engine given a URI and redirects to the Drupal URL.
   *
   * @param Request $request The HTTP request
   *
   * @return Response The redirect response to the Drupal URL
   */
  public function resolve(Request $request): Response
  {
    $logger = $this->getLogger('dexes_resolver');
    $uri    = $request->query->get('uri');

    if (NULL === $uri) {
      $logger->error('Missing required "uri" GET parameter');

      throw new NotFoundHttpException();
    }

    $result = $this->searchService->search($this->searchProfile, sprintf('sys_uri:"%s"', $uri));

    if (NULL === $result) {
      $logger->error(sprintf('Search engine was unavailable for resolving "%s"', $uri));

      throw new NotFoundHttpException();
    }

    $response = $result->getSolrResponse()->getResponse();

    if (NULL === $response) {
      $logger->error(sprintf('Malformed response retrieved from search engine while resolving "%s"', $uri));

      throw new NotFoundHttpException();
    }

    $documents = $response->getDocuments();

    if (0 === count($documents)) {
      $logger->info(sprintf('Uri "%s" was not found in search engine', $uri));

      throw new NotFoundHttpException();
    }

    $url = Url::fromRoute('dexes_dcat.dataset.view', [
      'dataset' => $documents[0]->getStringField('sys_name'),
    ])->toString();

    if ($url instanceof GeneratedUrl) {
      $url = $url->getGeneratedUrl();
    }

    return new RedirectResponse($url);
  }
}
