<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_resolver\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class SearchURIProfile.
 *
 * Search profile designed for resolving URI identifiers to the appropriate URL that renders the object attached to that
 * URI identifier.
 */
final class SearchURIProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select_dataset';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return ['sys_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'facet'      => FALSE,
      'spellcheck' => FALSE,
      'omitHeader' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'score DESC';
  }
}
