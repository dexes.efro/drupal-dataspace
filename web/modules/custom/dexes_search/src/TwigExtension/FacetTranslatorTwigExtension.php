<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

class FacetTranslatorTwigExtension extends AbstractExtension implements ExtensionInterface
{
  /**
   * @var string[] TODO: move to config?
   */
  public const FACET_LABELS = [
    'status'           => 'Status',
    'disclosure-level' => 'Disclosure level',
    'authority'        => 'Authority',
    'theme'            => 'Theme',
    'classification'   => 'Classification',
    'group'            => 'Group',
    'license'          => 'License',
    'keyword'          => 'Keyword',
    'language'         => 'Language',
    'format'           => 'Format',
    'frequency'        => 'Frequency',
    'catalog'          => 'Catalog',
    // TODO: rename in Solr and here to dataspace
    'community'        => 'Community',
    'recent'           => 'Recent',
    'appliance-type'   => 'Appliance type',
    'authority-type'   => 'Authority type',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFilters()
  {
    return [
      new TwigFilter('facet_translate', [$this, 'translateFacet']),
    ];
  }

  /**
   * Translates a facet field to a human friendly.
   *
   * @param string $facet The facet field to translate
   *
   * @return string The translated facet field
   */
  public function translateFacet(string $facet): string
  {
    if (array_key_exists($facet, self::FACET_LABELS)) {
      return self::FACET_LABELS[$facet];
    }

    return $facet;
  }
}
