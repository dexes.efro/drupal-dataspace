<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

class FieldValueToColor extends AbstractExtension implements ExtensionInterface
{
  /**
   * {@inheritdoc}
   */
  public function getFilters()
  {
    return [
      new TwigFilter('status_to_color', [$this, 'statusToColor']),
      new TwigFilter('access_rights_to_color', [$this, 'accessRightsToColor']),
      new TwigFilter('license_to_color', [$this, 'licenseToColor']),
    ];
  }

  /**
   * Returns to correct color based on the value of the status field.
   *
   * @param ?string $fieldValue The value of the status field
   *
   * @return string The color the chip should be
   */
  public function statusToColor(?string $fieldValue = NULL): string
  {
    if (is_null($fieldValue)) {
      return '';
    }

    $statusSuccessValues = [
      'http://data.overheid.nl/status/beschikbaar',
    ];
    $statusDangerValues = [
      'http://data.overheid.nl/status/niet_beschikbaar',
    ];
    if (in_array($fieldValue, $statusSuccessValues)) {
      return 'success';
    }
    if (in_array($fieldValue, $statusDangerValues)) {
      return 'danger';
    }

    return '';
  }

  /**
   * Returns to correct color based on the value of the status field.
   *
   * @param ?string $fieldValue The value of the status field
   *
   * @return string The color the chip should be
   */
  public function accessRightsToColor(?string $fieldValue = NULL): string
  {
    if (is_null($fieldValue)) {
      return '';
    }

    $statusSuccessValues = [
      'http://publications.europa.eu/resource/authority/access-right/PUBLIC',
    ];
    $statusDangerValues = [
      'http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC',
    ];
    if (in_array($fieldValue, $statusSuccessValues)) {
      return 'success';
    }
    if (in_array($fieldValue, $statusDangerValues)) {
      return 'danger';
    }

    return '';
  }

  /**
   * Returns to correct color based on the value of the status field.
   *
   * @param ?string $fieldValue The value of the status field
   *
   * @return string The color the chip should be
   */
  public function licenseToColor(?string $fieldValue = NULL): string
  {
    if (is_null($fieldValue)) {
      return '';
    }

    $statusSuccessValues = [
      'http://creativecommons.org/publicdomain/mark/1.0/deed.nl',
    ];
    $statusDangerValues = [
      'http://standaarden.overheid.nl/owms/terms/licentieonbekend',
      'http://standaarden.overheid.nl/owms/terms/geslotenlicentie',
    ];
    if (in_array($fieldValue, $statusSuccessValues)) {
      return 'success';
    }
    if (in_array($fieldValue, $statusDangerValues)) {
      return 'danger';
    }

    return '';
  }
}
