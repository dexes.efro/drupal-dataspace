<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\dexes_dataspace\Search\DataspaceBaseProfile;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

class SearchAllProfile extends DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return parent::standardFilters();
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return ['*'];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'hl'          => TRUE,
      'hl.fl'       => 'description',
      'hl.snippets' => 1,
      'hl.fragsize' => 150,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [
      'type'             => 'facet_sys_type',
      'language'         => 'facet_sys_language',
      'recent'           => 'facet_recent',
      'theme'            => 'facet_theme',
      // 'dataspace'        => 'facet_dataspace',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return ['score DESC', 'sys_modified DESC'];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    if (!empty($parser) && $parser->isDefaultQuery()) {
      return 'sys_modified DESC';
    }

    return 'score DESC';
  }
}
