<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\dexes_dataspace\Search\DataspaceBaseProfile;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

class AutocompleteProfile extends DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select_dataset';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return parent::standardFilters();
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 5;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [
      'sys_name',
      'sys_type',
      'title',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'hl'    => FALSE,
      'facet' => FALSE,
      'qf'    => 'title',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return ['score DESC', 'sys_modified DESC'];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'score DESC';
  }
}
