<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\dexes_dataspace\Search\DataspaceBaseProfile;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class RecentObjectsSearchProfile.
 *
 * Search profile for finding recent objects.
 */
class RecentObjectsSearchProfile extends DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return array_merge([
      'sys_type:("dataset" OR "dataservice")',
    ], parent::standardFilters());
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 12;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [
      'sys_type',
      'sys_modified',
      'sys_name',
      'title',
      'description',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'spellcheck' => FALSE,
      'facet'      => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'sys_modified DESC';
  }
}
