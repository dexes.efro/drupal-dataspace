<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\dexes_dataspace\Search\DataspaceBaseProfile;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class ThemeBlockSearchProfile.
 *
 * Search profile for finding all the themes for the homepage theme block.
 */
class ThemeBlockSearchProfile extends DataspaceBaseProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return parent::standardFilters();
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'spellcheck'               => FALSE,
      'f.facet_theme.facet.sort' => 'index',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'sys_id ASC';
  }
}
