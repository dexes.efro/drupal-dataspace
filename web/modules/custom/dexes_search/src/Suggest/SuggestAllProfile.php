<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Suggest;

use Drupal\xs_search\Suggest\SuggestProfileInterface;

class SuggestAllProfile implements SuggestProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'suggest';
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfSuggestions(): int
  {
    return 5;
  }

  /**
   * {@inheritdoc}
   */
  public function contextFilterQuery(): array
  {
    return ['self'];
  }
}
