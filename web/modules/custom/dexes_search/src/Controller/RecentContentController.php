<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Drupal\xs_solr\Search\Response\SearchDocumentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RecentContentController.
 *
 * Drupal controller for providing an AJAX endpoint exposing recent content.
 */
final class RecentContentController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchServiceInterface $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $searchProfile */
    $searchProfile = $container->get('dexes_search.recent_objects_profile');

    return new self($searchService, $searchProfile);
  }

  /**
   * RecentContentController constructor.
   *
   * @param SearchServiceInterface $searchService The service for interacting with the search engine
   * @param SearchProfileInterface $searchProfile The profile for retrieving catalog statistics
   */
  public function __construct(private SearchServiceInterface $searchService,
                              private SearchProfileInterface $searchProfile)
  {
  }

  /**
   * Retrieve the catalog statistics and return them as a JSON HTTP response.
   *
   * @return JsonResponse The appropriate JSON response
   */
  public function recentContent(): JsonResponse
  {
    return new JsonResponse([
      'content' => $this->getRecentObjects(),
    ]);
  }

  /**
   * Retrieve the recent content from the search engine.
   *
   * @return array<int, array<string, ?string>> The recent content
   */
  private function getRecentObjects(): array
  {
    $results = $this->searchService->search(
      $this->searchProfile, '*:*', [], 0,
      $this->searchProfile->sort()
    );

    if (empty($results)) {
      return [];
    }

    $response = $results->getSolrResponse()->getResponse();

    if (NULL === $response) {
      return [];
    }

    return array_map(function(SearchDocumentInterface $document) {
      return [
        'type'        => $document->getStringField('sys_type'),
        'title'       => $document->getStringField('title'),
        'description' => $document->getStringField('description'),
        'url'         => $this->createUrlForDocument($document),
      ];
    }, $response->getDocuments());
  }

  /**
   * Generate a URL to view the object behind a given search engine document.
   *
   * @param SearchDocumentInterface $document The document to generate the URL for
   *
   * @return string The generated URL
   */
  private function createUrlForDocument(SearchDocumentInterface $document): string
  {
    $urlOptions = [
      'absolute' => TRUE,
    ];

    if ('dataset' === $document->getStringField('sys_type')) {
      $url = Url::fromRoute('dexes_dcat.dataset.view', [
        'dataset' => $document->getStringField('sys_name'),
      ], $urlOptions)->toString();
    } elseif ('dataservice' === $document->getStringField('sys_type')) {
      $url = Url::fromRoute('dexes_dcat.dataservice.view', [
        'dataservice' => $document->getStringField('sys_name'),
      ], $urlOptions)->toString();
    } else {
      $url = Url::fromRoute('entity.node.canonical', [
        'node' => $document->getStringField('sys_id'),
      ], $urlOptions)->toString();
    }

    if ($url instanceof GeneratedUrl) {
      $url = $url->getGeneratedUrl();
    }

    return $url;
  }
}
