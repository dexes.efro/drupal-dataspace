<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CatalogStatisticsController.
 *
 * Drupal controller for providing an AJAX endpoint exposing various catalog statistics.
 */
final class CatalogStatisticsController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchServiceInterface $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $searchProfile */
    $searchProfile = $container->get('dexes_search.object_statistics_profile');

    return new self($searchService, $searchProfile);
  }

  /**
   * CatalogStatisticsController constructor.
   *
   * @param SearchServiceInterface $searchService The service for interacting with the search engine
   * @param SearchProfileInterface $searchProfile The profile for retrieving catalog statistics
   */
  public function __construct(private SearchServiceInterface $searchService,
                              private SearchProfileInterface $searchProfile)
  {
  }

  /**
   * Retrieve the catalog statistics and return them as a JSON HTTP response.
   *
   * @return JsonResponse The appropriate JSON response
   */
  public function statistics(): JsonResponse
  {
    return new JsonResponse([
      'statistics' => $this->getStatisticsPerObject(),
    ]);
  }

  /**
   * Retrieve the statistics per object from the search engine.
   *
   * @return array<string, array<string, int|string>> The statistics per object
   */
  private function getStatisticsPerObject(): array
  {
    $results = $this->searchService->search(
      $this->searchProfile, '*:*', [], 0,
      $this->searchProfile->sort()
    );

    if (empty($results)) {
      return [];
    }

    $facets = $results->getSolrResponse()->getFacets();

    if (empty($facets)) {
      return [];
    }

    $sys_type_facet = $facets->getFacetField('facet_sys_type');
    $objects        = [];

    foreach ($sys_type_facet as $name => $count) {
      $url = Url::fromRoute('dexes_search.search.all', [
        'scope'       => 'all',
        'query'       => '-',
        'filters'     => 'type:' . $name,
        'page_number' => 1,
      ])->toString();
      if ($url instanceof GeneratedUrl) {
        $url = $url->getGeneratedUrl();
      }
      $objects[$name] = [
        'count'      => $count,
        'search_url' => $url,
      ];
    }

    return $objects;
  }
}
