<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

final class AutocompleteController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchServiceInterface $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $searchProfile */
    $searchProfile = $container->get('dexes_search.autocomplete_profile');

    return new self($searchService, $searchProfile);
  }

  public function __construct(private SearchServiceInterface $searchService,
                              private SearchProfileInterface $autocompleteProfile)
  {
  }

  public function autocomplete(string $query = '*:*'): JsonResponse
  {
    $searchResponse = $this->searchService->search(
      $this->autocompleteProfile,
      $query,
      followSpellcheck: FALSE
    );

    if (NULL === $searchResponse) {
      return new JsonResponse([]);
    }

    $searchResults = $searchResponse->getSolrResponse()->getResponse();

    if (NULL === $searchResults) {
      return new JsonResponse([]);
    }

    $jsonData = [];

    foreach ($searchResults->getDocuments() as $document) {
      $jsonData[ucfirst($document->getStringField('sys_type') . 's')][] = [
        'title' => $document->getStringField('title'),
        'url'   => Url::fromRoute('dexes_dcat.dataset.view', [
          'dataset' => $document->getStringField('sys_name'),
        ], ['absolute' => TRUE])->toString(),
      ];
    }

    return new JsonResponse($jsonData);
  }
}
