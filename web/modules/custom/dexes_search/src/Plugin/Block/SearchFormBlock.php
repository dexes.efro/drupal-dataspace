<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Search datasets' Block.
 *
 * @Block(
 *   id = "dexes_search_block",
 *   admin_label = @Translation("Search block"),
 * )
 */
class SearchFormBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * The featured tags to render.
   *
   * @var array<int, array>
   */
  private array $featuredTags;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id,
                                                   $plugin_definition): self
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configFactory->get('dexes_search.featured_search_tags')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param ImmutableConfig $tags The featured tags to render
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $tags)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->featuredTags = [];

    foreach ($tags->getRawData() as $entry) {
      $this->featuredTags[] = $entry;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $selectedTags = $this->featuredTags;

    if (count($this->featuredTags) > 3) {
      shuffle($selectedTags);
      $selectedTags = array_slice($selectedTags, 0, 3);
    }

    return [
      'description'   => [
        '#markup' => $this->t('Search through a plethora of datasets and services'),
      ],
      'form'          => Drupal::formBuilder()
        ->getForm(Drupal\dexes_search\Form\SearchForm::class),
      'featured_tags' => $selectedTags,
    ];
  }
}
