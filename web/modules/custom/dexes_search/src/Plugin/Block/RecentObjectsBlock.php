<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'Recent Objects' Block.
 *
 * @Block(
 *   id = "dexes_recent_objects_block",
 *   admin_label = @Translation("Recent objects"),
 * )
 */
class RecentObjectsBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme'          => 'recent_objects',
      '#attached'       => [
        'drupalSettings' => [
          'dexes_search' => [
            'recentContent' => [
              'url'         => Url::fromRoute('dexes_search.ajax.recent_content')->toString(),
              'domSelector' => '.recentContentContainer',
            ],
          ],
        ],
        'library'        => [
          'dexes_search/recent_content',
        ],
      ],
    ];
  }
}
