<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Objects per Theme' Block.
 *
 * @Block(
 *   id = "dexes_objects_per_theme_block",
 *   admin_label = @Translation("Objects per theme"),
 * )
 */
class ObjectsPerThemeBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * The service for accessing the search engine.
   */
  private SearchServiceInterface $search_service;

  /**
   * The search profile to use to query for the available themes.
   */
  private SearchProfileInterface $search_profile;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id,
                                $plugin_definition): self
  {
    /** @var SearchServiceInterface $search_service */
    $search_service = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $search_profile */
    $search_profile = $container->get('dexes_search.theme_profile');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $search_service,
      $search_profile
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param SearchServiceInterface $searchService The service for accessing the search engine
   * @param SearchProfileInterface $searchProfile The search profile to use to query for the available themes
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              SearchServiceInterface $searchService, SearchProfileInterface $searchProfile)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->search_service = $searchService;
    $this->search_profile = $searchProfile;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme'             => 'objects_per_theme',
      '#objects_per_theme' => $this->getThemes(),
    ];
  }

  /**
   * Retrieve all the themes to show on the homepage from the search engine.
   *
   * @return array<string, int> The themes and their object counts
   */
  private function getThemes(): array
  {
    $results = $this->search_service->search(
      $this->search_profile, '*:*', [], 0,
      $this->search_profile->sort()
    );

    if (empty($results)) {
      return [];
    }

    $facets = $results->getSolrResponse()->getFacets();

    if (empty($facets)) {
      return [];
    }

    $theme_facet = $facets->getFacetField('facet_theme');
    $themes      = [];

    foreach ($theme_facet as $name => $count) {
      if (FALSE !== mb_strpos($name, '|')) {
        continue;
      }

      $themes[$name] = $count;
    }

    return $themes;
  }
}
