<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FeaturedSearchTagsForm.
 *
 * Provides the configuration form where one can define the featured search
 * results that will be shown below the search bar.
 */
class FeaturedSearchTagsForm extends ConfigFormBase
{
  /**
   * The name of the Drupal configuration entry holding the search tags.
   */
  private const CONFIG_NAME = 'dexes_search.featured_search_tags';

  /**
   * The cache tag invalidator to use.
   */
  protected CacheTagsInvalidatorInterface $tag_invalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');

    /** @var CacheTagsInvalidatorInterface $cache_invalidator */
    $cache_invalidator = $container->get('cache_tags.invalidator');

    return new FeaturedSearchTagsForm($config_factory, $cache_invalidator);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              CacheTagsInvalidatorInterface $tag_invalidator)
  {
    parent::__construct($config_factory);

    $this->tag_invalidator = $tag_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_search_featured_search_tags_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $entries      = array_values($this->config(self::CONFIG_NAME)->getRawData());
    $entriesCount = $form_state->get('entriesCount');

    if (NULL === $entriesCount) {
      $entriesCount = count(array_keys($entries)) + 1;
      $form_state->set('listsCount', $entriesCount);
    }

    $form['description'] = [
      '#markup' => $this->t('Enter the featured search tags in the form below. These entries will be displayed under the search bar when applicable. When more than three entries are given, a random selection of three entries will be shown under the search bar.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    $form['notice'] = [
      '#markup' => $this->t('Any URL can be used as a featured search tag, it is possible to configure entries that point to a specific dataset, rather than a search results page.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    $form['entries'] = [
      '#type'   => 'container',
      '#tree'   => TRUE,
      '#prefix' => '
        <div id="lists-wrapper">
          <table>
            <thead>
            <tr>
              <th>' . $this->t('Name') . '</th>
              <th>' . $this->t('URL') . '</th>
            </tr>
            </thead>
            <tbody>
      ',
      '#suffix' => '
            </tbody>
          </table>
        </div>
      ',
    ];

    for ($i = 0; $i < $entriesCount; ++$i) {
      $form['entries'][$i] = [
        'name' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Name'),
          '#title_display' => 'invisible',
          '#default_value' => $entries[$i]['name'] ?? NULL,
          '#prefix'        => '<tr><td>',
          '#suffix'        => '</td>',
        ],
        'url'  => [
          '#type'          => 'textfield',
          '#title'         => $this->t('URL'),
          '#title_display' => 'invisible',
          '#default_value' => $entries[$i]['url'] ?? NULL,
          '#prefix'        => '<td>',
          '#suffix'        => '</td>',
        ],
      ];
    }

    $form['row']['#type'] = 'actions';
    $form['row']['add']   = [
      '#type'                    => 'submit',
      '#value'                   => $this->t('Add another entry'),
      '#submit'                  => ['::addOne'],
      '#limit_validation_errors' => [],
      '#ajax'                    => [
        'callback' => '::addMoreCallback',
        'wrapper'  => 'lists-wrapper',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $newConfig = [];
    $entries   = $form_state->cleanValues()->getValue('entries', []);

    foreach ($entries as $entry) {
      if (empty($entry['name']) || empty($entry['url'])) {
        continue;
      }

      $newConfig[$entry['name']] = [
        'name' => $entry['name'],
        'url'  => $entry['url'],
      ];
    }

    $config = $this->config(self::CONFIG_NAME);
    $config->setData($newConfig);
    $config->save();

    $this->tag_invalidator->invalidateTags([
      'dexes_search',
      'featured_tags',
    ]);

    parent::submitForm($form, $form_state);
  }

  /**
   * Submit handler for the "add-one-more" button. Increments the max counter
   * and causes a rebuild.
   *
   * @param array<mixed, mixed> $form
   *                                        The form to apply the callback to
   * @param FormStateInterface  $form_state
   *                                        The current form state
   */
  public function addOne(array &$form, FormStateInterface $form_state): void
  {
    $form_state->set('entriesCount', $form_state->get('entriesCount') + 1);
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons. Selects and returns the fieldset
   * with the names in it.
   *
   * @param array<mixed, mixed> $form
   *                                        The form to apply the callback to
   * @param FormStateInterface  $form_state
   *                                        The current form state
   *
   * @return array<mixed, mixed>
   *                             The modified $form
   */
  public function addMoreCallback(array &$form,
                                  FormStateInterface $form_state): array
  {
    return $form['entries'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [self::CONFIG_NAME];
  }
}
