(function ($, Drupal, drupalSettings) {
  "use strict";

  let catalogStatisticsLoaded = false;

  function createStatisticsList(statistics) {
    const fragment = document.createDocumentFragment();
    const listWrapper = document.createElement("ul");
    listWrapper.className = "list-group-flush ml-0 pl-0";

    for (const [item, statistic] of Object.entries(statistics)) {
      const liElement = document.createElement("li");
      liElement.className = "list-group-item";

      const anchorElement = document.createElement("a");
      let typeLabel = item;

      if (statistic.count > 1) {
        typeLabel = typeLabel + "s";
      }

      anchorElement.href = statistic.search_url;
      anchorElement.innerHTML = Drupal.dexes_search.getTypeIcon(item) + Drupal.dexes_search.formatCount(statistic.count) + " " + Drupal.t(typeLabel);

      liElement.append(anchorElement);
      listWrapper.append(liElement);
    }

    fragment.append(listWrapper);

    return fragment;
  }

  Drupal.behaviors.catalogStatistics = {
    attach: function (context, settings) {
      if (catalogStatisticsLoaded) {
        return;
      }

      const config = drupalSettings.dexes_search.statistics;

      $.ajax({
        "url": config.url,
        "method": "GET",
        "success": function (data) {
          catalogStatisticsLoaded = true;
          $(config.domSelector).html(createStatisticsList(data.statistics));
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
