(function ($, Drupal, drupalSettings) {
  "use strict";

  let recentContentLoaded = false;

  function createRecentContentMarkup(recentContent) {
    const fragment = document.createDocumentFragment();

    for (const [index, recentContentItem] of Object.entries(recentContent)) {
      const itemWrapper = document.createElement("div");
      itemWrapper.className = "masonry-item";

      const itemAnchor = document.createElement("a");
      itemAnchor.className = "teaser-box hider";
      itemAnchor.href = recentContentItem.url;

      const itemBlock = document.createElement("div");

      const itemHeader = document.createElement("h3");
      itemHeader.className = "mb-2";
      itemHeader.innerHTML = Drupal.dexes_search.getTypeIcon(recentContentItem.type) + recentContentItem.title;

      const itemParagraph = document.createElement("p");
      itemParagraph.className = "teaser-box-clamp";
      itemParagraph.innerHTML = recentContentItem.description;

      itemBlock.append(itemHeader);
      itemBlock.append(itemParagraph);
      itemAnchor.append(itemBlock);
      itemWrapper.append(itemAnchor);
      fragment.append(itemWrapper);
    }

    return fragment;
  }

  Drupal.behaviors.recentContent = {
    attach: function (context, settings) {
      if (recentContentLoaded) {
        return;
      }

      const config = drupalSettings.dexes_search.recentContent;

      $.ajax({
        "url": config.url,
        "method": "GET",
        "success": function (data) {
          recentContentLoaded = true;
          $(config.domSelector).html(createRecentContentMarkup(data.content));
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
