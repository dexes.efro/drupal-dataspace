(function ($, Drupal, drupalSettings) {
  "use strict";

  let clearSuggesters = function(excludedNode = null) {
    let $suggestion_containers = $(".dexes_search_search_form_suggestions_container");

    if (null !== excludedNode) {
      $suggestion_containers = $suggestion_containers.not(excludedNode);
    }

    $suggestion_containers.empty();
    $suggestion_containers.css("display", "none");
  };

  Drupal.behaviors.autocomplete = {
    attach: function (context, settings) {
      $(".dexes_search_search_form_query").on("focus keyup", function() {
        let currentForm = $(this).closest("form");
        let container = currentForm.find(".dexes_search_search_form_suggestions_container");
        let value = this.value;

        clearSuggesters(container);

        if (value.length >= 2) {
          $.ajax({
            type: "GET",
            dataType: "json",
            url: settings.dexes_search.autocomplete.url + "/" + value
          }).done(function (data) {
            if (data.length === 0) {
              clearSuggesters();
              return;
            }

            const suggestionContainer = [];

            for (const [type, suggestions] of Object.entries(data)) {
              let suggestionMarkup = "";

              for (const [index, suggestion] of Object.entries(suggestions)) {
                suggestionMarkup = suggestionMarkup + "<li><a href=\"" + suggestion.url + "\">" + suggestion.title + "</a></li>";
              }

              suggestionContainer.push("<li>" + Drupal.t(type) + "<ul>" + suggestionMarkup + "</ul></li>");
            }

            container.html("<ul>" + suggestionContainer.join("") + "</ul>");
            container.css("display", "block");
          });
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
