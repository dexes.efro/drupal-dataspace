(function ($, Drupal, drupalSettings) {
  const numberFormatter = new Intl.NumberFormat(window.navigator.language);

  Drupal.dexes_search = {
    formatCount: function (counter) {
      return numberFormatter.format(counter);
    },

    getTypeIcon: function (contentType) {
      if (contentType in drupalSettings.dexes_search.icons) {
        return "<i class=\"fa fa-fw " + drupalSettings.dexes_search.icons[contentType] + " mr-2\"></i>";
      }

      return "";
    }
  };
}(jQuery, Drupal, drupalSettings));
