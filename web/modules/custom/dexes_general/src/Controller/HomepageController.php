<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_general\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class HomepageController.
 *
 * Responsible for all things pertaining to the homepage.
 */
final class HomepageController extends ControllerBase
{
  /**
   * Render the homepage on the screen.
   *
   * @return array<string, mixed> The render array for the homepage
   */
  public function showHomepage(): array
  {
    return [
      '#theme' => 'homepage',
    ];
  }
}
