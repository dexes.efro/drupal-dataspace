<?php

/**
 * This file is part of the dexes/drupal-dataspace project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_general\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class ExceptionSubscriber.
 *
 * Renders a 'pretty' HTTP 500 error page, rather than the standard Drupal
 * 'White Page of Death'. Note that not all errors can be handled by this
 * subscriber. PHP syntax errors will still result in a White Page of Death
 * for example.
 */
class ExceptionSubscriber extends HttpExceptionSubscriberBase
{
  /**
   * Render a custom HTTP 500 error page.
   *
   * It could be that Drupal Core itself is the cause of the event, in which
   * case using Drupal Core to render the custom page could itself cause
   * another Exception. Therefore, this implementation uses the minimum of
   * utilities provided by Drupal itself, as they may behave unexpectedly.
   *
   * @param ExceptionEvent $event The event to process
   */
  public function onException(ExceptionEvent $event): void
  {
    if ($event->getThrowable() instanceof HttpExceptionSubscriberBase
        || $event->getThrowable() instanceof HttpExceptionInterface) {
      // Only handle 'unhandled' Exceptions.
      return;
    }

    $event->setResponse(new Response(
      file_get_contents(__DIR__ . '/../../templates/500.html'),
      Response::HTTP_INTERNAL_SERVER_ERROR
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats()
  {
    return ['html'];
  }
}
