(function ($, Drupal) {
  Drupal.behaviors.layoutEvents = {
    // eslint-disable-next-line
    attach: function(context, settings) {
      /* Theme code */
      $(document).ready(function(){
        $(document).on("click", "#mobile_menu .yamm .nav-item .nav-link.dropdown-toggle:not(data-toggle)", function (e) {
          e.preventDefault();
          $(this).next().toggleClass("show");
        });
        $(document).on("click", "#mobile_menu .yamm .nav-item .nav-link.dropdown-toggle", function(){
          $("#mobile_menu").toggleClass("nav_open");
        });
        $(document).on("click", "#mobile_menu .fa-times-circle", function(){
          $("#mobile_menu").removeClass("nav_open");
          $("#mobile_menu .dropdown-menu").removeClass("show");
          $("#mobile_menu .yamm-content .col-6").removeClass("show");
          $("#mobile_menu .yamm-content").removeClass("menu_open");
        });
        $(document).on("click", "#mobile_menu .col-6 h4", function(){
          if( $(this).parent().attr("id") === "navMain" ) {
            var targetId = "#" + $(this).attr("aria-controls");
            $("#mobile_menu .yamm-content .col-6").removeClass("show");
            $(targetId).addClass("show");
          }
          $("#mobile_menu .yamm-content").toggleClass("menu_open");
        });
        $(document).on("click", ".toggle_offcanvas_menu", function(){
          $("body").toggleClass("offcanvas_menu_open");
        });
      });
      /* Drupal */
    }
  };
// eslint-disable-next-line
})(jQuery, Drupal);
