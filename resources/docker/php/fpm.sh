#!/bin/sh


set -eu


phive install --trust-gpg-keys E82B2FB314E9906E,CF1A108D0E7AE720,4AA394086372C20A


if [ -f ./composer.lock ]; then
    composer install --no-ansi --no-progress
else
    composer update
fi


wait-for-it dexspace-mariadb:3306 -s -t 60


DRUSH_PATH=/var/www/html/vendor/bin/drush


mkdir -p /var/www/html/web/sites/default


if ! ${DRUSH_PATH} status bootstrap | grep -q Successful; then
  cat > /var/www/html/drush/drush.yml << \EOF
options:
  uri: "https://dexspace.local"
EOF

  cat > /var/www/html/web/sites/default/settings.php << \EOF
<?php

// phpcs:ignoreFile

$settings['config_sync_directory'] = '/var/www/html/config/sync';
$settings['file_private_path'] = '/var/www/html/private';
$settings['file_temp_path'] = '/tmp';

$settings['hash_salt'] = 'GXCpFGNxrJbNgpN38Z3VpdkHzJQgDYnyXRaH2YgzplgeeLrKapVSa_Y6Pn7Z3X5DYuBWRNauCQ';
$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['entity_update_batch_size'] = 50;
$settings['entity_update_backup'] = TRUE;
$settings['migrate_node_migrate_type_classic'] = FALSE;

$settings['trusted_host_patterns'] = [
  '^dexspace\.local$',
  '^.+\.dexspace\.local$',
];

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$databases['default']['default'] = [
  'database'  => 'dexspace-drupal',
  'username'  => 'dexspace-drupal',
  'password'  => 'FooBarBaz',
  'prefix'    => '',
  'host'      => 'dexspace-mariadb',
  'port'      => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver'    => 'mysql',
];

$config['xs_api_client.settings']['is_managed'] = FALSE;
$config['xs_api_client.settings']['api_endpoint'] = 'https://api.staging.dexes.eu';

EOF

  ${DRUSH_PATH} site-install minimal \
    --account-name=dexspace_admin \
    --account-mail=support@dexes.nl \
    --account-pass=pass \
    --existing-config \
    --yes
fi


exec php-fpm
