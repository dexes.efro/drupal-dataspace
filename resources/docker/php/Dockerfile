FROM php:8.2-fpm

USER root

COPY --chown=root:root --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
      git \
      gpg && \
    pecl install xdebug && \
    apt-get clean && \
    rm --recursive --force /var/lib/apt/lists/*

RUN curl "https://phar.io/releases/phive.phar" \
      --silent \
      --show-error \
      --location \
      --output /usr/bin/phive && \
    curl "https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh" \
      --silent \
      --show-error \
      --location \
      --output /usr/bin/wait-for-it

COPY --chown=root:root ./resources/docker/php/fpm.sh /usr/bin/fpm

RUN chmod +x \
      /usr/bin/phive \
      /usr/bin/wait-for-it

ENV COMPOSER_CACHE_DIR="/var/www/html/.composer-cache" \
    PHIVE_HOME="/var/www/html/.phive-cache" \
    XDEBUG_MODE="coverage"

USER www-data
