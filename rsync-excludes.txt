/.git
/.idea
/.vscode

/bin
/drush/*
/private/*
/web/core/tests
/web/sites/default/files
/web/sites/default/settings.php
/web/sites/default/services.yml

/.editorconfig
/.gitattributes
/.gitignore
/.gitlab-ci.yml
/.php-cs-fixer.dist.php
/.jshintrc
/phpstan.neon.dist
/phpunit.xml.dist

/auth.json
/composer.lock

/rsync-excludes.txt
/resources
/docker-compose.yml

/*.md
/*.phar
